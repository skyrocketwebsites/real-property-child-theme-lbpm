<?php

/**
 * Single page for VIP Vendors
 *
 * This is a custom-built template, which is a combination of a bunch of other templates
 * ./tpl-feature.php (for the sidebar)
 * ./single.php (for the multiple columns)
 * ./single-dt_agencies.php (for the cool meta handling with icons)
 * Also, some legacy code from the old Estate theme.
 *
 * @author Nick Daugherty
 *
 */

get_header();

//$vip_resources_page = get_page_by_path('clients/vip-resources');

// Grab the current user's info
$display_name = $current_user->display_name;
$client_type = get_user_meta( $user_ID, 'client_type', true );

?>

	<!-- ** Primary Section ** -->
	<section id="primary" class="content-full-width">

		<div class="side-navigation">
		 	<div class="side-nav-container">
				<div class="side-nav-container">
					<?php switch ($client_type) {
						case "apt": case "comm": case "sfh":
							if(function_exists('dynamic_sidebar') && dynamic_sidebar(('concierge-menu-apt')) ): endif;
						break;
						case "hoa":
						default:
							if(function_exists('dynamic_sidebar') && dynamic_sidebar(('concierge-menu-hoa')) ): endif;

						break;
					} ?>
				</div>
		 	</div>
		 </div>

		<script type="text/javascript">
			// Highlight the VIP Resources menu item
			jQuery( ".side-navigation .widget ul.menu" ).addClass( "side-nav" );
			jQuery( ".menu .menu-item:contains(VIP Resources)" ).addClass("current_page_item");
		</script>

		<div class="side-navigation-content"><?php
			if( have_posts() ) :
			while ( have_posts() ) :
				the_post();

				//get_template_part( 'framework/loops/content', 'single' );
				?>

				<div class="dt-sc-agent-single">
					<div class="dt-sc-agents-list  dt-sc-agency-list">

						<div class="entry-thumb"><?php
							if( has_post_thumbnail() ):
								the_post_thumbnail("full");
							else: ?>
								<img src="http://placehold.it/400x420&text=Image" alt="<?php printf(esc_attr__('%s'),the_title_attribute('echo=0'));?>" title="<?php printf(esc_attr__('%s'),the_title_attribute('echo=0'));?>" /><?php
							endif;?>
						</div>

						<div class="entry-details">
						<div class="entry-details-inner">
							<div class="entry-title">
								<h4><?php the_title();?></h4>
							</div>


							<div class="dt-sc-agent-contact"><?php
								$agent_name = get_post_meta ( $post->ID, "_name",true);
								$agent_title = get_post_meta ( $post->ID, "_title",true);
								$agency_address = get_post_meta ( $post->ID, "_address",true);
								$agency_mobile = get_post_meta ( $post->ID, "_mobile",true);
								$agency_phone = get_post_meta ( $post->ID, "_phone",true);
								$agency_email_id = get_post_meta ( $post->ID, "_email",true);
								$agency_website = get_post_meta ( $post->ID, "_website",true);

								if( !empty($agent_name) )
									echo "<p> <span class='fa fa-user'> </span> {$agent_name}";
									echo (!empty($agent_title)) ? ", ".$agent_title : "";
									echo "</p>";

								if( !empty( $agency_mobile ) )
									echo "<p> <span class='fa fa-mobile'> </span> {$agency_mobile} </p>";

								if( !empty( $agency_phone ) )
									echo "<p> <span class='fa fa-phone'> </span> {$agency_phone} </p>";

								if( !empty( $agency_email_id ) )
									echo "<p> <span class='fa fa-envelope'> </span> <a href='mailto:{$agency_email_id}'>{$agency_email_id}</a></p>";

								if( !empty($agency_address) )
									echo "<p> <span class='fa fa-map-marker'> </span> {$agency_address} </p>";

								if( !empty($agency_website) )
									echo "<p> <span class='fa fa-globe'> </span> <a href='{$agency_website}' target='_blank'>View Website</a> </p>";

								?>

							</div>

							<div class="dt-sc-agent-content"> <?php the_content();?></div>

									<!-- <br>SOCIAL GOES HERE<br> -->
							<?php $socials =	get_post_meta( $post->ID, "_agency_social",true);
								if( !empty($socials) ):?>
									<ul class="dt-sc-social-icons"><?php
										foreach( $socials as $k => $v ):
											$i1 = IAMD_BASE_URL."images/sociable/hover/{$k}";
											$i2 = IAMD_BASE_URL."images/sociable/{$k}";
											$class = explode(".",$k);
											$class = $class[0];
											echo "<li class='{$class}'><a href='{$v}'><img src='{$i1}'/><img src='{$i2}'/></a></li>";
										endforeach;?>
									</ul><?php
								endif;?>
							<div class="clear"></div>
						</div>
						</div>
					</div>
				</div><?php

				edit_post_link( __( ' Edit ','dt_themes' ) );
				endwhile;
			endif;?>
		</div>
	</section><!-- ** Primary Section End ** -->

<?php get_footer(); ?>