<?php
/*-----------------------------------------------------------------------------------*/
/* Setup */
/*-----------------------------------------------------------------------------------*/
$includes_path = get_stylesheet_directory() . '/includes/';				// Set path to theme specific functions

require_once ($includes_path . 'custom-hooks.php');         // Custom hooks we're using
require_once ($includes_path . 'custom-shortcodes.php');	// Custom Shortcodes
require_once ($includes_path . 'custom-page-subtitle-section.php'); // Custom page title section

//require_once ($includes_path . 'custom-functions.php');		// Custom Helper Functions
//require_once ($includes_path . 'property-reviews.php');		// Custom property reviews loop

/*-----------------------------------------------------------------------------------*/
/* Load Custom Scripts */
/*-----------------------------------------------------------------------------------*/

add_action( 'wp_enqueue_scripts', 'lbpm_custom_scripts', 101 );
function lbpm_custom_scripts() {

    // use the latest version of font awesome instead of the version packaged with the theme
    wp_dequeue_style( 'custom-font-awesome' );
    wp_enqueue_style ('font-awesome-latest', '//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css');

	// custom JS, includes several overwrites of the default version packaged with the theme
    wp_enqueue_script( 'lbpm-custom-js', get_stylesheet_directory_uri() .'/js/custom.js', array('jquery'), false, true);

	// gravity forms styling tweaks, we enqueue it later
    wp_register_script( 'gform-script', get_stylesheet_directory_uri() .'/js/custom-forms-min.js', array('jquery'), "1", true);
    //wp_enqueue_script( 'gform-script' );

	// custom CSS for shortcodes
    wp_enqueue_style('shortcode-colors', get_stylesheet_directory_uri() . '/css/shortcodes.css');

}

add_action( 'after_setup_theme', 'lbpm_undo_parent_theme_hooks' );
function lbpm_undo_parent_theme_hooks() {
    // Parent theme has a filter to modify the default category widget
    // Turning it off, since we're not using that widget, and it interferes with VIP Resources page
    remove_filter('wp_list_categories', 'my_wp_list_categories');
}

// adding our own HOA and APT sidebars for the concierge
register_sidebar(array(
	'name' 			=>	'Concierge Menu (HOA)',
	'id'			=>	'concierge-menu-hoa',
	'description'	=>	__("Left sidebar that appears on the concierge page template","dt_themes"),
	'before_widget' => 	'<aside id="%1$s" class="widget %2$s">',
	'after_widget' 	=> 	'</aside>',
	'before_title' 	=> 	'<h3 class="widgettitle">',
	'after_title' 	=> 	'<span></span></h3>'
	)
);
register_sidebar(array(
	'name' 			=>	'Concierge Menu (APT)',
	'id'			=>	'concierge-menu-apt',
	'description'	=>	__("Left sidebar that appears on the concierge page template","dt_themes"),
	'before_widget' => 	'<aside id="%1$s" class="widget %2$s">',
	'after_widget' 	=> 	'</aside>',
	'before_title' 	=> 	'<h3 class="widgettitle">',
	'after_title' 	=> 	'<span></span></h3>'
	)
);
