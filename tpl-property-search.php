<?php /*Template Name: Property Search Result Template */
get_header();

	$tpl_default_settings = get_post_meta( $post->ID, '_tpl_default_settings', TRUE );
	$tpl_default_settings = is_array( $tpl_default_settings ) ? $tpl_default_settings  : array();

	$page_layout  = array_key_exists( "layout", $tpl_default_settings ) ? $tpl_default_settings['layout'] : "content-full-width";
	$show_sidebar = $show_left_sidebar = $show_right_sidebar =  false;
	$sidebar_class = "";

	switch ( $page_layout ) {
		case 'with-left-sidebar':
			$page_layout = "page-with-sidebar with-left-sidebar";
			$show_sidebar = $show_left_sidebar = true;
			$sidebar_class = "secondary-has-left-sidebar";
		break;

		case 'with-right-sidebar':
			$page_layout = "page-with-sidebar with-right-sidebar";
			$show_sidebar = $show_right_sidebar	= true;
			$sidebar_class = "secondary-has-right-sidebar";
		break;

		case 'both-sidebar':
			$page_layout = "page-with-sidebar page-with-both-sidebar";
			$show_sidebar = $show_right_sidebar	= $show_left_sidebar = true;
			$sidebar_class = "secondary-has-both-sidebar";
		break;

		case 'content-full-width':
		default:
			$page_layout = "content-full-width";
		break;
	}


	#Search Page Settings
	$post_layout = isset( $tpl_default_settings['product-search-layout'] ) ? $tpl_default_settings['product-search-layout'] : "one-half-column";
	$post_per_page = isset($tpl_default_settings['product-post-per-page']) ? $tpl_default_settings['product-post-per-page'] : -1;


	$gactive = 	$lactive = $post_class = "";

	switch($post_layout):
		case 'one-column':
			$post_class = $show_sidebar ? " column dt-sc-one-column with-sidebar blog-fullwidth" : " column dt-sc-one-column blog-fullwidth";
			$columns = 1;
			$gactive = 'active';
		break;

		case 'one-half-column';
			$post_class = $show_sidebar ? " column dt-sc-one-half with-sidebar" : " column dt-sc-one-half";
			$columns = 2;
			$gactive = 'active';
		break;

		case 'one-third-column':
			$post_class = $show_sidebar ? " column dt-sc-one-third with-sidebar" : " column dt-sc-one-third";
			$columns = 3;
			$gactive = 'active';
		break;

		case 'one-fourth-column':
			$post_class = $show_sidebar ? " column dt-sc-one-fourth with-sidebar" : " column dt-sc-one-fourth";
			$columns = 4;
			$gactive = 'active';
		break;

		case 'list-view':
			$post_class = "property-list-view";
			$columns = 0;
			$lactive = 'active';
		break;
	endswitch;

			/* Front End View changer */
			if( isset($_REQUEST['view']) && $_REQUEST['view'] === "list" ) {
				$post_class = "property-list-view";
				$post_layout = "list-view";
				$columns = 0;
				$lactive = 'active';
				$gactive = '';
			} elseif( isset($_REQUEST['view']) && $_REQUEST['view'] === "grid" ) {
				$gactive = 'active';
				$lactive = '';
				if( $post_layout === 'list-view'){
					$post_layout = "grid";
					$post_class = $show_sidebar ? "  column dt-sc-one-third with-sidebar " : " column dt-sc-one-third";
					$columns = 3;
				}
			}
			/* Front End View Changer */
	if ( $show_sidebar ):
		if ( $show_left_sidebar ): ?>
			<!-- Secondary Left -->
			<section id="secondary-left" class="secondary-sidebar <?php echo $sidebar_class;?>"><?php get_sidebar( 'left' );?></section><?php
		endif;
	endif;?>

	<!-- ** Primary Section ** -->
	<section id="primary" class="<?php echo $page_layout;?>"><?php
		$current_url = $_SERVER["REQUEST_URI"];
		$current_url = str_replace(array("&view=grid" , "&view=list"),"",$current_url);?>

			<div class="dt-sc-hr-invisible"></div>

			<div class="property-view-type">
				<a href="<?php echo $current_url;?>&view=grid" class="property-grid-type <?php echo $gactive;?>"> <span> </span><?php _e('Grid','dt_themes');?></a>
				<a href="<?php echo $current_url;?>&view=list" class="property-list-type <?php echo $lactive;?>"> <span> </span><?php _e('List','dt_themes');?></a>
			</div>
			<div class="dt-sc-clear"></div><?php

		$paged    = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
		$posts_per_page = $tpl_default_settings['product-post-per-page'];
		$wp_query = new WP_Query();

		$properties = array(
			'post_type'=>'dt_properties',
			'posts_per_page'=>$post_per_page,
			'paged'=>$paged,
			'tax_query'=>array(),
			'meta_query'=>array(),
			'order_by'=> 'published');

		#Post Title
		if( isset($_REQUEST['pname']) ):

			# search_proprty_title - custom arg please refer property_title_filter()
			$properties['search_proprty_title'] = $_REQUEST['pname'];
			add_filter( 'posts_where', 'property_title_filter', 10, 2 );
		endif;

		#Contract Type
		if( isset($_REQUEST['searchby'])  && $_REQUEST['searchby'] !== "default" ):

            $contract_type_id = get_term_by('slug',$_REQUEST['searchby'],'contract_type',ARRAY_A);
            $contract_type_id = is_array( $contract_type_id ) ? $contract_type_id['term_id'] : "";


			$properties['tax_query'][] = array( 'taxonomy' => 'contract_type',
				'field' => 'id',
				'terms' => $contract_type_id,
				'operator' => 'IN',);
		endif;

		#Location
		if( !empty( $_REQUEST['plocation']) && $_REQUEST['plocation'] > 0 ) {
			$properties['tax_query'][] = array( 'taxonomy' => 'property_location',
				'field' => 'id',
				'terms' => $_REQUEST['plocation'],
				'operator' => 'IN',);
		}

		#Property Type
		if( !empty( $_REQUEST['ptype']) && $_REQUEST['ptype'] > 0 ) {
			$properties['tax_query'][] = array( 'taxonomy' => 'property_type',
				'field' => 'id',
				'terms' => $_REQUEST['ptype'],
				'operator' => 'IN',);
		}

		# Beds Meta
		if( !empty( $_REQUEST['pbeds']) && $_REQUEST['pbeds'] > 0 ) {
			$properties['meta_query'][] = array(
				'key'     => '_bedrooms',
				'value'   => $_REQUEST['pbeds'],
				'compare' => '>=',
				'type'    => 'numeric',);
		}

		#Bath Meta
		if( !empty( $_REQUEST['pbaths']) && $_REQUEST['pbaths'] > 0 ) {
			$properties['meta_query'][] = array(
				'key'     => '_bathrooms',
				'value'   => $_REQUEST['pbaths'],
				'compare' => '>=',
				'type'    => 'numeric',);
		}

		#Floors Meta
		if( !empty( $_REQUEST['pfloors']) && $_REQUEST['pfloors'] > 0 ) {
			$properties['meta_query'][] = array(
				'key'     => '_floors',
				'value'   => $_REQUEST['pfloors'],
				'compare' => '>=',
				'type'    => 'numeric',);
		}

		#Parking Meta
		if( !empty( $_REQUEST['pgarages']) && $_REQUEST['pgarages'] > 0 ) {
			$properties['meta_query'][] = array(
				'key'     => '_parking',
				'value'   => $_REQUEST['pgarages'],
				'compare' => '>=',
				'type'    => 'numeric',);
		}

		#Price
		$minprice = isset($_REQUEST['minprice']) ? $_REQUEST['minprice'] : 0;
		$maxprice = isset($_REQUEST['maxprice']) ? $_REQUEST['maxprice'] : 0;
		if( $minprice > 0 && $maxprice > 0 ){

			$properties['meta_query'][] = array(
				'key'     => '_property_price',
				'value'   => array( $minprice, $maxprice ),
				'type'    => 'numeric',
				'compare' => 'BETWEEN');

		}elseif( $minprice > 0 ){

			$properties['meta_query'][] = array(
				'key'     => '_property_price',
				'value'   => $minprice,
				'type'    => 'numeric',
				'compare' => '>=');

		}elseif( $maxprice > 0 ){

			$properties['meta_query'][] = array(
				'key'     => '_property_price',
				'value'   => $maxprice,
				'type'    => 'numeric',
				'compare' => '<=');
		}

		$wp_query->query( $properties );

		remove_filter( 'posts_where', 'property_title_filter', 10, 2 ); # Used to search properties based on title

		$price_suffix = dttheme_option("property","currency");
		$areaunit = dttheme_option("property","area_unit");

		if( $wp_query->have_posts() ):

			$i = 1;

			while( $wp_query->have_posts() ):
				$wp_query->the_post();

				$temp_class = "";
				$temp_class = ( $i == 1 ) ? "{$post_class} first" : $post_class;
				$i = ( $i == $columns ) ? 1 : $i+1;

				$the_id = get_the_ID();
				$permalink = get_permalink($the_id);
				$title = get_the_title($the_id);

				$contract_type = "";
				$contract_type_slug = "";
				$contract_type_link = "";
				$contract = get_the_terms( $the_id, 'contract_type' );
				if( is_object( $contract) || is_array($contract) ){
					foreach ( $contract as $c ) :
						$contract_type = $c->name;
						$contract_type_slug = $c->slug;
						$contract_type_link = get_term_link( $contract_type_slug, 'contract_type' );
					endforeach;
				}

				$property_type = "";
				$property_type_link = "";
				$property_type = get_the_terms( $the_id, 'property_type' );
				if( is_object( $property_type) || is_array($property_type) ){
					foreach ( $property_type as $c ) :
						$property_type = $c->name;
						$property_type_link = get_term_link( $property_type, 'property_type' );
					endforeach;
				}

				// SKYROCKET - ADDED CODE HERE TO USE OUR DATABASE KEYS
				// INSTEAD OF REALPROPERTY DEFAULT KEYS
				//$video = get_post_meta ( $the_id, "embed",true);

				//echo "<pre>"; print_r($images); echo "</pre>";

				//@TODO - Add price back in?
				//$price = get_post_meta ( $the_id, "_property_price",true);?>

				<!-- Property Item -->
				<?php if( $post_layout !== "list-view" ): ?>
				<div class="column <?php echo $temp_class;?>">
			    <?php endif;?>

					<div class="property-item <?php if( $post_layout === 'list-view'){ echo $post_class; } ;?>">

						<div class="property-thumb">
							<?php //Contract type is the top left ribbon
								//@TODO - add contract_type back in?
							/*if( !empty( $contract_type ) ):?>
								<span class="property-contract-type <?php echo $contract_type_slug;?>">
									<?php echo"<a href='{$contract_type_link}'>{$contract_type}</a>";?>
								</span>
							<?php endif;
							*/

							if ( has_post_thumbnail() ) : ?>
								<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
									<?php the_post_thumbnail('thumbnail'); ?>
								</a>
							<?php else: ?>
								<img src='http://placehold.it/338x200&text=Photo Not Available' alt='' title=''/>
							<?php endif; ?>

							<div class="property-thumb-meta">
								<?php
								//@TODO - Add property_type and price back in?
								/*
								if( !empty($property_type) )
									echo "<span class='property-type'><a href='{$property_type_link}'>{$property_type}</a></span>";

								if( !empty($price) ) {
									echo "<span class='property-price'>";
									echo ($property_type == 'Apartment') ? "From " : "";
									echo "{$price_suffix} {$price} </span>";
								}
								*/ ?>
							</div>
						</div>

						<div class="property-details">
							<div class="property-details-inner">
								<h2><a href='<?php echo $permalink;?>'><?php echo $title;?></a></h2>
								<h3><?php echo get_post_meta ( $the_id, "address2",true);?></h3>
								<?php if($post_layout === 'list-view' )
										the_excerpt();?>

									<ul class="property-meta">
										<?php /*
											//@TODO - Add property meta in (figure out which fields are relevant to show)
											$area = get_post_meta ( $the_id, "_area",true);
											$bedrooms = get_post_meta ( $the_id, "_bedrooms",true);
											$bathrooms = get_post_meta ( $the_id, "_bathrooms",true);
											$floors = get_post_meta ( $the_id, "_floors",true);
											$parking = get_post_meta ( $the_id, "_parking",true);
											if( !empty($area) )
												echo "<li>{$area}{$areaunit}<span>".__('Area','dt_themes').'</span></li>';

											if( !empty($bedrooms) )
												echo "<li>{$bedrooms}<span>".__('Beds','dt_themes')."</span></li>";

											if( !empty($bathrooms) )
												echo "<li>{$bathrooms}<span>".__('Baths','dt_themes')."</span></li>";

											if( !empty($floors) )
												echo "<li>{$floors}<span>".__('Floors','dt_themes')."</span></li>";

											if( !empty($parking) )
												echo "<li>{$parking}<span>".__('Garages','dt_themes')."</span></li>";

											if($post_layout === 'list-view' )
												echo "<li class='read-more'><a href='{$permalink}'>".__('More Details','dt_themes')." <i class='fa fa-angle-double-right'></i></a></li>";
											*/?>
									</ul>
							</div>
						</div>

					</div>

				<?php if( $post_layout !== "list-view" ): ?>
				</div>
			    <?php endif;?>
				<!-- Property Item End-->
			<?php endwhile;
		else:
		endif;?>

		<!-- **Pagination** -->
		<div class="pagination">
			<div class="prev-post"><?php previous_posts_link('<span class="fa fa-angle-double-left"></span> Prev');?></div>
			<?php echo dttheme_pagination();?>
			<div class="next-post"><?php next_posts_link('Next <span class="fa fa-angle-double-right"></span>');?></div>
		</div><!-- **Pagination - End** -->

	</section><!-- ** Primary Section End ** --><?php

	if ( $show_sidebar ):
		if ( $show_right_sidebar ): ?>
			<!-- Secondary Right -->
			<section id="secondary-right" class="secondary-sidebar <?php echo $sidebar_class;?>"><?php get_sidebar( 'right' );?></section><?php
		endif;
	endif;?>
<?php get_footer(); ?>