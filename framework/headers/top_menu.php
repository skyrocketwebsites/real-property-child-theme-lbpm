<div class="top-bar">
    <div class="container"><?php
        if (function_exists('wp_nav_menu')) :
            $topMenu = wp_nav_menu(array('theme_location'=>'top_menu','menu_id'=>'','menu_class'=>'top-menu','container'=>false, 'depth' => 1, 'fallback_cb'=>'dttheme_default_navigation'));
        endif;
        if(!empty($topMenu))    echo $topMenu;?>

        <div class="top-right">
            <ul class="user-account"><?php
                  if( is_user_logged_in() ):
				  	if( dttheme_is_plugin_active('s2member/s2member.php') ):
						$login_welcome_page = get_option('ws_plugin__s2member_cache');
						$login_welcome_page = $login_welcome_page['login_welcome_page'];
						$page = $login_welcome_page['page'];
						if( !empty($page) ):
							$link = $login_welcome_page['link'];
							$title =  get_the_title($page);
							echo "<li><a href='{$link}'><span class='fa fa-home'> </span>{$title}</a></li>";
						endif;
					endif;

                    //$dashboard = dt_get_page_permalink_by_its_template('tpl-dashboard.php');
                    //$add = is_null($dashboard) ? "#" : $dashboard."?action=add";
                    //$dashboard =  is_null($dashboard) ? home_url()."/wp-admin/post-new.php?post_type=page" : $dashboard;
                    $dashboard = home_url() . '/clients/dashboard/';
                    ?>
                    <li>
                        <a href="<?php echo $dashboard; ?>">
                            <span class="fa fa-dashboard"> </span>
                            <?php _e('Dashboard','dt_themes');?>
                        </a>
                    </li>
<!--
                    <li>
                        <a href="<?php echo $add; ?>">
                            <span class="fa fa-plus-square"> </span>
                            <?php _e('Add Property','dt_themes');?>
                        </a>
                    </li>
-->
                    <li>
                        <a href="<?php echo wp_logout_url( home_url() ); ?>">
                            <span class="fa fa-user"> </span>
                            <?php _e('Logout','dt_themes');?>
                        </a>
                    </li>
            <?php else: // logged out
                    $login = dt_get_page_permalink_by_its_template('tpl-login.php');
                    $login = is_null($login) ? home_url()."/wp-login.php" : $login;?>
                    <li>
                        <a href="<?php echo $login;?>"> <span class="fa fa-user"> </span>
                        <?php _e('Client Login','dt_themes');?>
                        </a>
                    </li>
            <?php endif;?>
            </ul>

            <?php if( dttheme_option('appearance','contact-number') ): ?>
            	<div class="contact-number">
                	<span class="fa fa-phone-square"> </span> <?php echo dttheme_option('appearance','contact-number');?>
                </div>
            <?php endif;?>
            <?php do_action('icl_language_selector'); ?>
        </div>
    </div>
</div>