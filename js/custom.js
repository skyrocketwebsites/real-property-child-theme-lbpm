jQuery(document).ready(function($){

	// Sticky header styling
	if( navigator.platform.match(/(Mac|iPhone|iPod|iPad)/i) ||
		navigator.userAgent.match(/Android/i)||
		navigator.userAgent.match(/webOS/i) ||
		navigator.userAgent.match(/iPhone/i) ||
		navigator.userAgent.match(/iPod/i)) {
		if( mytheme_urls.stickynav === "enable") {
			if ($(window).width() < 479) {
				   $("#header-wrapper").sticky({ topSpacing: -33 });
			}
			else {
				   $("#header-wrapper").sticky({ topSpacing: -43 });
			}
		}
	} else {
		if( mytheme_urls.stickynav === "enable") {
			$("#header-wrapper").sticky({ topSpacing: 0 });
		}
	}

	// Run FitVids plugin on bg landing pages
    $(".bg-landing").fitVids();
});

