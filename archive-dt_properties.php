<?php get_header();
	$page_layout  = dttheme_option( 'property', 'property-archive-page-layout' );
	$page_layout  = !empty( $page_layout ) ? $page_layout : "with-left-sidebar";
	$show_sidebar = $show_left_sidebar = $show_right_sidebar =  false;
	$sidebar_class = "";
	$price_suffix = dttheme_option("property","currency");
	$areaunit = dttheme_option("property","area_unit");

	switch ( $page_layout ) {

		case 'with-left-sidebar':
			$page_layout = "page-with-sidebar with-left-sidebar";
			$show_sidebar = $show_left_sidebar = true;
			$sidebar_class = "secondary-has-left-sidebar";
		break;

		case 'with-right-sidebar':
			$page_layout = "page-with-sidebar with-right-sidebar";
			$show_sidebar = $show_right_sidebar = true;
			$sidebar_class = "secondary-has-right-sidebar";
		break;

		case 'both-sidebar':
			$page_layout = "page-with-sidebar page-with-both-sidebar";
			$show_sidebar = $show_right_sidebar = $show_left_sidebar = true;
			$sidebar_class = "secondary-has-both-sidebar";
		break;

		case 'content-full-width':
		default:
			$page_layout = "content-full-width";
		break;
	}

	if ( $show_sidebar ):
		if ( $show_left_sidebar ): ?>
			<!-- Secondary Left -->
			<section id="secondary-left" class="<?php echo $sidebar_class;?>"><?php get_sidebar( 'left' );?></section><?php
		endif;
	endif;?>
		<section id="primary" class="<?php echo $page_layout;?>"><?php

			$post_layout = dttheme_option('property','property-archive-post-layout');
			$post_layout = !empty($post_layout) ? $post_layout : "one-half-column";
			$gactive = 	$lactive = $post_class = "";

			switch($post_layout):

				case 'one-column':
					$post_class = $show_sidebar ? " column dt-sc-one-column with-sidebar" : " column dt-sc-one-column ";
					$columns = 0;
					$gactive = 'active';
				break;

				case 'one-half-column';
					$post_class = $show_sidebar ? "  column dt-sc-one-half with-sidebar " : "  column dt-sc-one-half ";
					$columns = 2;
					$gactive = 'active';
				break;

				case 'one-third-column':
					$post_class = $show_sidebar ? "  column dt-sc-one-third with-sidebar " : "  column dt-sc-one-third ";
					$columns = 3;
					$gactive = 'active';
				break;

				case 'one-fourth-column':
					$post_class = $show_sidebar ? "  column dt-sc-one-fourth with-sidebar " : " column dt-sc-one-fourth";
					$columns = 4;
					$gactive = 'active';
				break;

				case 'list-view':
					$post_class = "property-list-view";
					$columns = 0;
					$lactive = 'active';
				break;
			endswitch;

			/* Front End View changer */
			if( isset($_REQUEST['view']) && $_REQUEST['view'] === "list" ) {
				$post_class = "property-list-view";
				$post_layout = "list-view";
				$columns = 0;
				$lactive = 'active';
				$gactive = '';
			} elseif( isset($_REQUEST['view']) && $_REQUEST['view'] === "grid" ) {
				$gactive = 'active';
				$lactive = '';
				if( $post_layout === 'list-view'){
					$post_layout = "grid";
					$post_class = $show_sidebar ? "  column dt-sc-one-third with-sidebar " : " column dt-sc-one-third";
					$columns = 3;
				}
			} ?>

			<div class="property-view-type">
				<a href="?view=grid" class="property-grid-type <?php echo $gactive;?>"> <span> </span><?php _e('Grid','dt_themes');?></a>
				<a href="?view=list" class="property-list-type <?php echo $lactive;?>"> <span> </span><?php _e('List','dt_themes');?></a>
			</div>
			<div class="dt-sc-clear"></div>

			<?php
			/* Front End View changer */

			if( have_posts() ):
				$i = 1;
				while( have_posts() ):
					the_post();
					if($i == 1) $temp_class = $post_class." first"; else $temp_class = $post_class;
					if($i == $columns) $i = 1; else $i = $i + 1;

					$the_id = get_the_ID();
					$permalink = get_permalink($the_id);
					$title = get_the_title($the_id);

					$contract_type = "";
					$contract_type_slug = "";
					$contract_type_link = "";
					$contract = get_the_terms( $the_id, 'contract_type' );
					if( is_object( $contract) || is_array($contract) ) {
						foreach ( $contract as $c ) :
							$contract_type = $c->name;
							$contract_type_slug = $c->slug;
							$contract_type_link = get_term_link( $contract_type_slug, 'contract_type' );
						endforeach;
					}

					$property_type = "";
					$property_type_link = "";
					$property_type = get_the_terms( $the_id, 'property_type' );
					if( is_object( $property_type) || is_array($property_type) ){
						foreach ( $property_type as $c ) :
							$property_type = $c->name;
							$property_type_link = get_term_link( $property_type, 'property_type' );
						endforeach;
					}

					$media = get_post_meta ( $the_id, "_property_media",true);
					$media = is_array($media) ? $media : array();
					$price = get_post_meta ( $the_id, "_property_price",true);?>

					<?php if( $post_layout !== "list-view" ): ?>
					<div class="<?php echo $temp_class;?>">
					<?php endif;?>

						<div class="property-item <?php if( $post_layout === 'list-view'){ echo $post_class; } ;?>">

							<div class="property-thumb">
								<?php /*if( !empty( $contract_type ) ):?>
										<span class="property-contract-type <?php echo $contract_type_slug;?>"><?php echo"<a href='{$contract_type_link}'>{$contract_type}</a>";?></span>
								<?php endif; */

								if ( has_post_thumbnail() ) : ?>
								<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
									<?php the_post_thumbnail('thumbnail'); ?>
								</a>
							<?php else: ?>
								<img src='http://placehold.it/338x200&text=Photo Not Available' alt='' title=''/>
							<?php endif; ?>

								<div class="property-thumb-meta"><?php /*
									if( !empty($property_type) )
										echo "<span class='property-type'><a href='{$property_type_link}'>{$property_type}</a></span>";

									if( !empty($price) )
										echo "<span class='property-price'>{$price_suffix} {$price} </span>"; */?>
								</div>
							</div>

							<div class="property-details">
								<div class="property-details-inner">
									<h2><a href='<?php echo $permalink;?>'><?php echo $title;?></a></h2>
									<h3><?php echo get_post_meta ( $the_id, "_property_address",true);?></h3>
									<?php if($post_layout === 'list-view' ) the_excerpt();?>

									<ul class="property-meta"><?php /*
											$area = get_post_meta ( $the_id, "_area",true);
											$bedrooms = get_post_meta ( $the_id, "_bedrooms",true);
											$bathrooms = get_post_meta ( $the_id, "_bathrooms",true);
											$floors = get_post_meta ( $the_id, "_floors",true);
											$parking = get_post_meta ( $the_id, "_parking",true);
											if( !empty($area) )
												echo "<li>{$area}{$areaunit}<span>".__('Area','dt_themes').'</span></li>';

											if( !empty($bedrooms) )
												echo "<li>{$bedrooms}<span>".__('Beds','dt_themes')."</span></li>";

											if( !empty($bathrooms) )
												echo "<li>{$bathrooms}<span>".__('Baths','dt_themes')."</span></li>";

											if( !empty($floors) )
												echo "<li>{$floors}<span>".__('Floors','dt_themes')."</span></li>";

											if( !empty($parking) )
												echo "<li>{$parking}<span>".__('Garages','dt_themes')."</span></li>";

											if($post_layout === 'list-view' )
												echo "<li class='read-more'><a href='{$permalink}'>".__('More Details','dt_themes')." <i class='fa fa-angle-double-right'></i></a></li>"; */?>
									</ul>
								</div>
							</div>
						<?php if( $post_layout !== "list-view" ): ?>
						</div>
					    <?php endif;?><!-- Property Item End-->
					</div><?php
				endwhile;
			else:?>
				<div class="dt-sc-hr-invisible"> </div>
				<h1><?php _e( 'Nothing Found','dt_themes'); ?></h1>
				<h3><?php _e( 'Apologies, but no results were found for the requested archive.', 'dt_themes'); ?></h3>
				<?php get_search_form();
			endif;?>
	        <div class="dt-sc-clear"></div>

			<!-- **Pagination** -->
			<div class="pagination">
				<div class="prev-post"><?php previous_posts_link('<span class="fa fa-angle-double-left"></span> Prev');?></div>
				<?php echo dttheme_pagination();?>
				<div class="next-post"><?php next_posts_link('Next <span class="fa fa-angle-double-right"></span>');?></div>
			</div><!-- **Pagination - End** -->
		</section><?php
	if ( $show_sidebar ):
		if ( $show_right_sidebar ): ?>
			<!-- Secondary Right -->
			<section id="secondary-right" class="<?php echo $sidebar_class;?>"><?php get_sidebar( 'right' );?></section><?php
		endif;
	endif;
get_footer(); ?>