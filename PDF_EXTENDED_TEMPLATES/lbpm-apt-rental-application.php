<?php

/**
 * Don't give direct access to the template
 */
if(!class_exists("RGForms")){
	return;
}

/**
 * Set up the form ID and lead ID
 * Form ID and Lead ID can be set by passing it to the URL - ?fid=1&lid=10
 */
 PDF_Common::setup_ids();

/**
 * Load the form data to pass to our PDF generating function
 */
$form = RGFormsModel::get_form_meta($form_id);

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <link rel='stylesheet' href='<?php echo PDF_PLUGIN_URL .'initialisation/template.css'; ?>' type='text/css' />
    <!--link rel='stylesheet' href='<?php get_template_directory_uri(); ?>/PDF_EXTENDED_TEMPLATES/lbpm-template.css' type='text/css' /-->
	<?php
		/*
		 * Create your own stylesheet or use the <style></style> tags to add or modify styles
		 * The plugin stylesheet is overridden every update
		 */
	?>
	<style>
		@page {
			 margin: 4mm 8mm 0;
			 margin-header: 0;
			 margin-footer: 0;
		}
		body { font-size: 13px; }
		a { color: #377DEB; text-decoration: none;}
		.center {text-align: center;}
		.even {background: #f2f2f2;}
		td {padding: 2px;}
		section h2 {
			width: 100%;
			font-size: 12px;
			line-height: 1em;
			margin: 0;
			margin-top: 10px;
			padding-top: 5px;
			height: 16px;
			text-transform: uppercase;
		}

		.label { font-size: 10px; text-transform: uppercase; letter-spacing: 1px; vertical-align: middle; color: #888; }
		th.label { letter-spacing: 0; color: #000; background: none; text-align: left; font-size:12px; }
		th.left-header { vertical-align: top; width: 53mm; padding-top: 5px; }
		.first table.prop-details { margin-bottom: 5px; font-size: 12px;}
		.first .prop-details, .first .client-details { width: 100%;}
		.first .prop-details .col1, .first .client-details .col1 { width: 35mm; }
		.first .prop-details .col2, .first .client-details .col2 { width: 80mm; }
		.first .prop-details .col3, .first .client-details .col3 { width: 45mm; }
		table { width: 100%; }
		.residence-history table thead tr th,
		.employment-history table .current,
		.employment-history table .previous,
		.bank-info table thead tr th,
		.proposed-occupants table thead tr th,
		.emergency-contacts table thead tr th,
		.pets table thead tr th,
		.automobiles table thead tr th {
			background: #4A858F;
			color: #fff;
			font-weight: normal;
			font-size: 12px;
			padding: 5px;
			text-align: left;
		}
		.residence-history table thead tr .col1 { width: 45mm;}
		.residence-history table thead tr .col2 { width: 40mm;}
		.residence-history table thead tr .col3 { width: 30mm;}
		.residence-history table tbody tr td,
		.employment-history table tbody tr td,
		.bank-info table tbody tr td,
		.proposed-occupants table tbody tr td,
		.emergency-contacts table tbody tr td,
		.pets table tbody tr td,
		.automobiles table tbody tr td {
			background: #eee;
			font-size: 11px;
			height: 18px;
			padding: 2px 4px;
		}
		.employment-history table .current,
		.employment-history table .previous {
			width: 72mm;
		}
		.employment-history table .label { background: none; }
		.proposed-occupants {
			width: 50mm;
			padding-right: 2%;
			float: left;
		}
		.authorization p {
			font-size: 10px;
			line-height: 1.5em;
			margin-top: 0;
		}
		.signing table tbody tr td {
			height: 10px;
			font-size: 16px;
			padding-left: 5px;
			border-bottom: 2px solid #ccc;
		}
		.signing table tbody tr td .signature {
			width: auto;
		}
		.signing table tfoot tr td {
			padding: 2px 5px 0;
			color: #B0B0AF;
		}
		.signing table tfoot tr td.col1 { width: 20%;}
		.signing table tfoot tr td.col3 { width: 25%;}
		.signing table tfoot tr td.col4 { width: 30%;}
		.signing table tfoot tr td span { font-size: 13px; color: #ddd !important; }
	</style>

    <title>LBPM Apartment Rental Application</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
	<body>

      <?php

      foreach($lead_ids as $lead_id) {

          $lead = RGFormsModel::get_lead($lead_id);
          $form_data = GFPDFEntryDetail::lead_detail_grid_array($form, $lead);

		/*
		 * Add &data=1 when viewing the PDF via the admin area to view the $form_data array
		 */
		PDF_Common::view_data($form_data);

		/*
		 * Store your form fields from the $form_data array into variables here
		 * To see your entire $form_data array, view your PDF via the admin area and add &data=1 to the url
		 *
		 * For an example of accessing $form_data fields see http://gravityformspdfextended.com/documentation-v3-x-x/templates/getting-started/
		 *
		 * Alternatively, as of v3.4.0 you can use merge tags (except {allfields}) in your templates.
		 * Just add merge tags to your HTML and they'll be parsed before generating the PDF.
		 *
		 */
		 $applicant_types = explode('/', $form_data['field'][4]);
		 $applicant_type = $applicant_types[0];

		?>
		<header>
			<h1 class="center">
				<img src="//lbpm.com/wp-content/uploads/2015/05/LBPM-Apt-Rental-Application-logo.png"  />
			</h1>
		</header>

		<section class="first">
			<table class="prop-details">
				<tbody>
					<tr>
						<td class="col1 label">Property</td>
						<td class="col2">
							<?php
								// property id
								echo ($form_data['field'][74]) ? $form_data['field'][74] : $form_data['field'][29];
								// unit number
								echo ($form_data['field'][97]) ? ' - Unit ' . $form_data['field'][97] : "";
							?>
						</td>
						<td class="col3 label">Req. Move-In Date</td>
						<td class="col4"><?php echo $form_data['field'][96]; ?></td>

					</tr>
				</tbody>
			</table>

			<table class="client-details">
				<tbody>
					<tr>
						<td class="col1 label"><?php echo $applicant_type; ?> Name</td>
						<td class="col2"><?php echo $form_data['field'][94]; ?></td>
						<td class="col3 label">Social Security #</td>
						<td class="col4"><?php echo $form_data['field'][5]; ?></td>

					</tr>
					<tr>
						<td class="col1 label">Home Phone</td>
						<td class="col2">{Phone:73}</td>
						<td class="col3 label">Driver's License #</td>
						<td class="col4"><?php echo $form_data['field'][6]; ?></td>
					</tr>
					<tr>

						<td class="col1 label">Email</td>
						<td class="col2"><?php echo $form_data['field'][3]; ?></td>
						<td class="col3 label">Date of Birth</td>
						<td class="col4"><?php echo $form_data['field'][9]; ?></td>
					</tr>
					<tr>
						<td></td>
					</tr>
				</tbody>
			</table>
		</section>

		<section class="residence-history">
			<h2>Residence History (Past 2 Years)</h2>
			<table>
				<thead>
					<tr>
						<th class="col1">Address</th>
<!-- 						<th class="col2">City</th> -->
						<th class="col2">Manager Name</th>
						<th class="col3">Manager Phone</th>
						<th class="col4">Move In</th>
						<th class="col5">Move Out</th>
						<th class="col6">Reason for Moving</th>
					</tr>
				</thead>
				<tbody>
					<tr><!-- Their Current Address -->
						<td>
							<?php echo $form_data['field'][10]['street']; ?><br />
							<?php echo $form_data['field'][10]['city']; ?>, <?php echo $form_data['field'][10]['zip']; ?>
						</td>
						<td><?php echo $form_data['field'][11]; ?></td>
						<td><?php echo $form_data['field'][41]; ?></td>
						<td><?php echo $form_data['field'][12]; ?></td>
						<td><?php echo $form_data['field'][13]; ?></td>
						<td><?php echo $form_data['field'][76]; ?></td>
					</tr>
					<tr><!-- Their Previous Address -->
						<td>
							<?php echo $form_data['field'][36]['street']; ?><br />
							<?php echo $form_data['field'][36]['city']; ?>, <?php echo $form_data['field'][36]['zip']; ?>
						</td>
						<td><?php echo $form_data['field'][40]; ?></td>
						<td><?php echo $form_data['field'][37]; ?></td>
						<td><?php echo $form_data['field'][42]; ?></td>
						<td><?php echo $form_data['field'][43]; ?></td>
						<td><?php echo $form_data['field'][77]; ?></td>
					</tr>
				</tbody>
			</table>
		</section>
		<br />

		<section class="employment-history">
			<table>
				<thead>
					<tr>
						<th class="label">Employment History</th>
						<th class="current">Current</th>
						<th class="previous">Previous</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="label">Employer</td>
						<td><?php echo $form_data['list'][80][0]['Current']; ?></td>
						<td><?php echo $form_data['list'][80][0]['Previous']; ?></td>
					</tr>
					<tr>
						<td class="label">Dates of Employment</td>
						<td><?php echo $form_data['list'][82][0]['Current']; ?></td>
						<td><?php echo $form_data['list'][82][0]['Previous']; ?></td>
					</tr>
					<tr>
						<td class="label">Type of Work</td>
						<td><?php echo $form_data['list'][87][0]['Current']; ?></td>
						<td><?php echo $form_data['list'][87][0]['Previous']; ?></td>
					</tr>
					<tr>
						<td class="label">Gross Monthly Salary</td>
						<td><?php echo $form_data['list'][86][0]['Current']; ?></td>
						<td><?php echo $form_data['list'][86][0]['Previous']; ?></td>
					</tr>
					<tr>
						<td class="label">Business Address</td>
						<td><?php echo $form_data['list'][85][0]['Current']; ?></td>
						<td><?php echo $form_data['list'][85][0]['Previous']; ?></td>
					</tr>
					<tr>
						<td class="label">Business Phone</td>
						<td><?php echo $form_data['list'][84][0]['Current']; ?></td>
						<td><?php echo $form_data['list'][84][0]['Previous']; ?></td>
					</tr>
					<tr>
						<td class="label">Supervisor</td>
						<td><?php echo $form_data['list'][83][0]['Current']; ?></td>
						<td><?php echo $form_data['list'][83][0]['Previous']; ?></td>
					</tr>
				</tbody>
				<tfoot>
					<tr>
						<td></td>
					</tr>
				</tfoot>
			</table>
		</section>

		<section class="bank-info">
			<h2>Bank Information</h2>
			<?php echo $form_data['field'][92]; ?>
		</section>

		<section class="other-contacts">
			<div class="proposed-occupants">
				<h2>Proposed Occupants</h2>
				<?php echo $form_data['field'][15]; ?>
			</div>
			<div class="emergency-contacts">
				<h2>Emergency Contacts</h2>
				<table>
					<thead>
						<tr>
							<th>Name</th>
							<th>Relation</th>
							<th>Phone #</th>
						</tr>
					</thead>
					<tbody>
						<tr><!-- Cosigner -->
							<td><?php echo $form_data['field'][46]; ?></td>
							<td><?php if ( $form_data['field'][56] ) echo "Cosigner ( {$form_data['field'][56]} )"; ?></td>
							<td></td>
						</tr>
						<tr><!-- Other Emergency Contact -->
							<td><?php echo $form_data['field'][95]; ?></td>
							<td><?php echo $form_data['field'][52]; ?></td>
							<td><?php echo $form_data['field'][53]; ?></td>
						</tr>
					</tbody>
				</table>
			</div>
		</section>
		<br />

		<section class="pets">
			<table>
				<tr>
					<th class="label left-header">Pets</th>
					<td><?php echo $form_data['field'][34]; ?></td>
				</tr>
			</table>
		</section>

		<section class="automobiles">
			<table>
				<tr>
					<th class="label left-header">Automobiles</th>
					<td><?php echo $form_data['field'][24]; ?></td>
				</tr>
			</table>
		</section>

		<section class="authorization">
			<h2>Authorization</h2>
			<!-- <p>Applicant represents that all information and statements provided on this application are true and correct, and hereby authorizes verification of references to include but not limited to credit checks, Unlawful Detainer checks and telecredit checks, and agrees to furnish additional credit references upon request, and waives any claim against any person(s) providing such verification. Applicant further agrees to pay for said verification upon submission of application. Applicant understands that falsification of any of the information or statements provided on this application will be grounds for rejection of application and forfeiture of any deposits held for subject apartment</p> -->
			<p><?php echo $form_data['field']['25_name'][0]; ?></p>
		</section>

		<section class="signing">
			<table>
				<tbody>
					<tr>
						<td><?php echo $form_data['field'][94]; ?></td>
						<td class="signature"><?php echo $form_data['signature'][0]; ?></td>
						<td><?php echo $form_data['misc']['date_time']; ?></td>
						<td><?php echo $form_data['misc']['transaction_id']; ?></td>
					</tr>
				</tbody>
				<tfoot>
					<tr>
						<td class="col1"><?php echo $applicant_type; ?> Name</td>
						<td class="col2"><?php echo $applicant_type; ?> Signature</td>
						<td class="col3">Date</td>
						<td class="col3">Payment ID (<?php echo $form_data['products_totals']['total_formatted']; ?>)</td>
					</tr>
				</tfoot>
			</table>

		</section>

		<pagebreak />

		<!-- Page 2 -->
		<header>
			<h1 class="center">
				<img src="//lbpm.com/wp-content/uploads/2015/05/LBPM-Apt-Rental-Application-logo-p2.png"  />
			</h1>
		</header>

		<section class="page2">
			<div id="identification">
				<h2>Two Forms of ID:</h2>
				<ol class="id-list">
				<?php
					$ids = $form_data['field']['93'];
					foreach ($ids as $url)
					{
						// get the last part of the URL
						$path = parse_url($url, PHP_URL_PATH);
						$pathFragments = explode('/', $path);
						$end = end($pathFragments);

						echo "<li><a target='_blank' href='$url'>$end</a></li>";
					}
				?>
				</ol>
			</div>

			<div id="employment">
				<h2>Proof of Employment</h2>
				<ol class="paystubs-list">
				<?php
					$stubs = $form_data['field']['88'];
					foreach ($stubs as $url)
					{
						// get the last part of the URL
						$path = parse_url($url, PHP_URL_PATH);
						$pathFragments = explode('/', $path);
						$end = end($pathFragments);

						echo "<li><a target='_blank' href='$url'>$end</a></li>";

					}
				?>
				</ol>
			</div>

			<div id="bank-statements">
				<h2>Bank Statements</h2>
				<ol class="statements-list">
				<?php
					$statements = $form_data['field']['89'];
					foreach ($statements as $url)
					{
						// get the last part of the URL
						$path = parse_url($url, PHP_URL_PATH);
						$pathFragments = explode('/', $path);
						$end = end($pathFragments);

						echo "<li><a target='_blank' href='$url'>$end</a></li>";

					}
				?>
				</ol>

			</div>
		</section>

		<pagebreak />

		<?php
			// CHRISTIAN - THIS CODE JUST GENERICALLY LOOPS THROUGH ALL THE FIELDS AND SPITS THEM ALL OUT
			/* generate the entry HTML */
        GFPDFEntryDetail::lead_detail_grid($form, $lead, $config_data['empty_field'], $config_data['html_field'], $config_data['page_names']);
      }

    ?>
	</body>
</html>