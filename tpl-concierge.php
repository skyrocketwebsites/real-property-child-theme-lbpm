<?php
/**
 * Template Name: Concierge Template
 * Description: Custom page template to use for the LBPM Client Concierge pages.
 *
 * @author Nick Daugherty
 */

get_header();

// Grab the current user's info
$display_name = $current_user->display_name;
$client_type = get_user_meta( $user_ID, 'client_type', true );

?>

	<!-- ** Primary Section -->
	<section id="primary" class="content-full-width">

		<div class="side-navigation">
			<div class="side-nav-container">
				<?php switch ($client_type) {
						case "apt": case "comm": case "sfh":
							if(function_exists('dynamic_sidebar') && dynamic_sidebar(('concierge-menu-apt')) ): endif;
						break;
						case "hoa":
						default:
							if(function_exists('dynamic_sidebar') && dynamic_sidebar(('concierge-menu-hoa')) ): endif;

						break;
				} ?>
			</div>
		</div>

		<script type="text/javascript">
			jQuery( ".side-navigation .widget ul.menu" ).addClass( "side-nav" );
		</script>

		<!-- ** Primary Section ** -->
		<div class="side-navigation-content"><?php
			if( have_posts() ):
				while( have_posts() ):
					the_post();
					get_template_part( 'framework/loops/content', 'page' );
				endwhile;
			endif;?>
		</div>
	</section><!-- ** Primary Section End ** -->

<?php get_footer(); ?>