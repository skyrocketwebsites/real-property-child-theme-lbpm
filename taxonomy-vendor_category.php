<?php

/**
 * Category page for VIP Vendors
 *
 * This is a custom-built taxonomy template, which is a combination of a bunch of other templates
 * ./tpl-feature.php (for the sidebar)
 * ./tpl-blog.php (for the multiple columns)
 * Also, some legacy code from the old Estate theme.
 *
 * @author Nick Daugherty
 *
 */

get_header();

//$vip_resources_page = get_page_by_path('clients/vip-resources');

// Grab the current user's info
$display_name = $current_user->display_name;
$client_type = get_user_meta( $user_ID, 'client_type', true );


?>

	<!-- ** Primary Section ** -->
	<section id="primary" class="content-full-width">

		<div class="side-navigation">
		 	<div class="side-nav-container">
				<div class="side-nav-container">
					<?php switch ($client_type) {
						case "apt": case "comm": case "sfh":
							if(function_exists('dynamic_sidebar') && dynamic_sidebar(('concierge-menu-apt')) ): endif;
						break;
						case "hoa":
						default:
							if(function_exists('dynamic_sidebar') && dynamic_sidebar(('concierge-menu-hoa')) ): endif;

						break;
					} ?>
				</div>
		 	</div>
		 </div>

		<script type="text/javascript">
			// Highlight the VIP Resources menu item
			jQuery( ".side-navigation .widget ul.menu" ).addClass( "side-nav" );
			jQuery( ".menu .menu-item:contains(VIP Resources)" ).addClass("current_page_item");
		</script>

		<div class="side-navigation-content"><?php
			if( have_posts() ):
				$i = 1; $col = 1;
				while( have_posts() ):
					the_post();

					// Logic to create the 3 different post sizes
					if ($i == 1) {
						$post_class = "column dt-sc-one-column blog-fullwidth";
						$columns = 1;
						$button_text = "Get Contact Info";
						//$container_class = "";
					}
					elseif ( $i == 2 || $i == 3) {
						$post_class = "column dt-sc-one-half";
						$columns = 2;
						$button_text = "Get Contact Info";
						//$container_class = "apply-isotope";
					}
					else {
						$post_class = "column dt-sc-one-fourth";
						$columns = 4;
						$button_text = "Contact";
						//$container_class = "apply-isotope";
					}

					$temp_class = "";

					// Column logic
					if ($col == 1) $temp_class = $post_class." first"; else $temp_class = $post_class;
					if($col == $columns) $col = 1; else $col++;
					?>

					<div class="<?php echo $temp_class;?> ">
						<!-- #post-<?php the_ID()?> starts -->
						<article id="post-<?php the_ID(); ?>" <?php post_class('blog-entry'); ?>>
							<div class="blog-entry-inner">

								<div class="entry-thumb">

									<a href="<?php the_permalink();?>" title="<?php printf(esc_attr__('%s'),the_title_attribute('echo=0'));?>">
									<?php if( has_post_thumbnail() ):
											the_post_thumbnail("full");
										  else:?>
										  	<img src="http://placehold.it/1060x636&text=Image" alt="<?php printf(esc_attr__('%s'),the_title_attribute('echo=0'));?>" title="<?php printf(esc_attr__('%s'),the_title_attribute('echo=0'));?>" />
									<?php endif;?>
									</a>

								</div>

								<div class="entry-details">

									<div class="entry-details-inner">

										<?php if( $i == 1 ): ?>
											<div class="featured-post"> <span class="fa fa-trophy"> </span> <span class="text"> <?php _e('Featured','dt_themes');?> </span></div>
										<?php endif;?>

										<div class="entry-title">
											<h4>
												<a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( '%s'), the_title_attribute( 'echo=0' ) ); ?>"><?php the_title(); ?></a>
											</h4>
										</div>

										<?php if ( $i <= 3 ): ?>
											<div class="entry-body"><?php echo dttheme_excerpt(100);?></div>
										<?php endif; ?>

										<a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__('%s'), the_title_attribute('echo=0'));?>" class="dt-sc-button filled with-icon small read-more">
											<?php
												if ( $i <= 3)
													echo '<i class="fa fa-angle-double-right"> </i>';

												echo $button_text;
											?>
										</a>

									</div>
								</div>
							</div>
						</article><!-- #post-<?php the_ID()?> Ends -->
					</div>

				<?php
				$i++; // increment the $i variable

				endwhile;
			endif;?>
		</div>
	</section><!-- ** Primary Section End ** -->

<?php get_footer(); ?>