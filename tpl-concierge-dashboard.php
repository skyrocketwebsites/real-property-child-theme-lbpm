<?php
/**
 * Template Name: Concierge Dashboard
 * Description: Custom page template to use for the LBPM Client Concierge dashboard.
 * This pretty much a non-elegant quick hack to output the correct markup to create the page.
 *
 * @author Nick Daugherty
 */

// Setup the data we'll need below
global $current_user;
get_currentuserinfo();

// If user is not logged in, redirect them to the login page.
if ($user_ID == 0) {
	$login = dt_get_page_permalink_by_its_template('tpl-login.php');
	$login = is_null($login) ? home_url()."/wp-login.php" : $login;
	wp_redirect( $login );
	exit;
}

// Grab the current user's info
$display_name = $current_user->display_name;
$client_type = get_user_meta( $user_ID, 'client_type', true );


get_header();
?>

<!-- ** Primary Section ** -->
<section id="primary" class="content-full-width">
	<?php
	if ( have_posts() ):
		while ( have_posts() ):
			the_post(); ?>

			<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<?php the_content(); ?>
			</div><!-- #post-<?php the_ID(); ?> -->
	<?php
	endwhile;
	endif;?>

	<div class="concierge-dashboard">

		<!-- ** First Row ** -->
		<div class="column dt-sc-two-sixth space first">
			<div class="dt-sc-colored-box space" style="background:#0FACCE;">
				<h5><span class="fa fa-wrench"> </span>Maintenance Tickets</h5>

				<p>Keep tabs on all the maintenance items happening at your property.</p>
				<a href="//lbpm.com/clients/maintenance/" title="View your current maintenance tickets." class="dt-sc-button small">View Tickets</a>
				<!-- <a href="//lbpm.com/clients/maintenance/new" title="Create a new maintenance ticket." class="dt-sc-button small">New Ticket</a> -->
			</div>
		</div>

		<div class="column dt-sc-two-sixth space">
			<div class="dt-sc-colored-box space" style="background:#595CA1;">
				<h5><span class="fa fa-comment"> </span>Concierge Requests</h5>

				<p>Find updated information on all the concierge tickets from your property.</p>
				<a href="//lbpm.com/clients/concierge/" title="View your current concierge requests" class="dt-sc-button small">View Tickets</a>
				<!-- <a href="//lbpm.com/clients/concierge/new" title="Create a new concierge request." class="dt-sc-button small">New Ticket</a> -->
			</div>
		</div>

		<?php switch ($client_type) {
			case "apt": case "comm": case "sfh":
				$box3 = array(
					'title' => "Properties",
					'icon' => "fa-building-o",
					'description' => "View your property websites.",
					'tooltip' => "View your property websites.",
					'url' => site_url( '/clients/my-properties/', 'https' ),
				);
				$box4 = array(
					'title' => "Vacancies",
					'icon' => "fa-pie-chart",
					'description' => "Current occupancy &amp; open vacancies.",
					'tooltip' => "View your current occupancy and open vacancies.",
					'url' => site_url( '/clients/vacancies/', 'https' ),
				);
				$box8 = array(
					'url' => 'http://lbpmreviews.com',
				);
				break;

			case "hoa":
			default:
				$box3 = array(
					'title' => "Delinquency",
					'icon' => "fa-clock-o",
					'description' => "Current delinquencies for your property.",
					'tooltip' => "View current delinquencies for your property.",
					'url' => site_url( '/clients/delinquencies/', 'https' ),
				);
				$box4 = array(
					'title' => "Violations",
					'icon' => "fa-exclamation-triangle",
					'description' => "Current homeowner violations.",
					'tooltip' => "View the current homeowner violations.",
					'url' => site_url( '/clients/violations/', 'https' ),
				);
				$box8 = array(
					'url' => 'http://lbpmreviews.com/hoa-board-members/',
				);
				break;
		} ?>

		<div class="column dt-sc-one-sixth space">
			<div class="dt-sc-colored-box space" style="background:#7d888e;">
				<h5><span class="fa <?php echo $box3['icon']; ?>"> </span><?php echo $box3['title']; ?></h5>

				<p><?php echo $box3['description']; ?></p>

				<a class="dt-sc-button small" title="<?php echo $box3['tooltip']; ?>" href="<?php echo $box3['url']; ?>" >View</a>
			</div>
		</div>

		<div class="column dt-sc-one-sixth space">
			<div class="dt-sc-colored-box space" style="background:#b31f41;">
				<h5><span class="fa <?php echo $box4['icon']; ?>"> </span><?php echo $box4['title']; ?></h5>

				<p><?php echo $box4['description']; ?></p>

				<a class="dt-sc-button small" title="<?php echo $box4['tooltip']; ?>" href="<?php echo $box4['url']; ?>" >View</a>
			</div>
		</div>

		<div class="dt-sc-hr-invisible"></div>

		<!-- ** Second Row ** -->
		<div class="column dt-sc-two-sixth space first">
			<div class="dt-sc-colored-box space" style="background:#d36b5e;">
				<h5><span class="fa fa-check-square-o"> </span>Action Items</h5>

				<p>Our shared to-do list for your property. Used for large projects or initiatives.</p>
				<a href="//lbpm.com/clients/action-items/" class="dt-sc-button small">Current Items</a>
				<!-- <a href="//lbpm.com/clients/action-items/new" class="dt-sc-button small">New Item</a> -->
			</div>
		</div>

		<div class="column dt-sc-two-sixth space">
			<div class="dt-sc-colored-box space" style="background:#34495E;">
				<h5><span class="fa fa-archive"> </span>Documents</h5>

				<p>The latest operating statements for your property, along with a historical archive.</p>
				<a href="//lbpm.com/clients/statements/" class="dt-sc-button small">Statements</a>
				<a href="//lbpm.com/clients/documents/" class="dt-sc-button small">Other Docs</a>
			</div>
		</div>

		<div class="column dt-sc-two-sixth last space">
			<div class="dt-sc-colored-box space" style="background:#81c77f;">
				<h5><span class="fa fa-thumbs-o-up"> </span>VIP Resources</h5>

				<p>We’ve negotiated with some of our top vendors to bring you these special offers.</p>
				<a href="//lbpm.com/clients/vip-resources/" class="dt-sc-button small">Access Top Vendors</a>
			</div>
		</div><br>

		<div class="dt-sc-hr-invisible"></div>

		<!-- ** Third Row ** -->
		<div class="column dt-sc-two-sixth space first">
			<div class="dt-sc-colored-box space" style="background:#478bca;">
				<h5><span class="fa fa-yelp"> </span>Reviews from Clients</h5>

				<p>“Out of all the property management companies we’ve dealt with, LBPM has to be one of the…”</p>
				<a href="http://lbpm.com/reviews/" class="dt-sc-button small">Read More</a>
				<a href="<?php echo $box8['url']; ?>" class="dt-sc-button small">Write A Review</a>
			</div>
		</div>

		<div class="column dt-sc-two-sixth space">
			<div class="dt-sc-colored-box space" style="background:#1abc9c;">
				<h5><span class="fa fa-university"> </span>Mortgage Services</h5>

				<p>Our hand-picked mortgage professionals will evaluate your property free of charge.</p>
				<a href="//lbpm.com/mortgages/" class="dt-sc-button small">Learn More</a>
			</div>
		</div>

		<div class="column dt-sc-one-sixth space">
			<div class="dt-sc-colored-box space" style="background:#614051;">
				<h5><span class="fa fa-question-circle"> </span>Help</h5>

				<p>Find answers to common questions.</p>
				<a href="//lbpm.com/clients/frequently-asked-questions/" class="dt-sc-button small">Help</a>
			</div>
		</div>

		<div class="column dt-sc-one-sixth space">
			<div class="dt-sc-colored-box space" style="background:#536878;">
				<h5><span class="fa fa-user"> </span>Edit Profile</h5>

				<p>Update your email and password.</p>
				<a href="//lbpm.com/clients/profile/" class="dt-sc-button small">Edit</a>
			</div>
		</div>

	</div><!-- ** Concierge Dashboard End ** -->

	<div class="clear"></div>
	<?php edit_post_link( __( ' Edit ','dt_themes' ) );?>
</section><!-- ** Primary Section End ** -->

<?php get_footer(); ?>

