<?php get_header();

// get meta fields
$phone = get_field('tracking_phone') ?: get_field('bldg_phone');
$manager = get_field('resident_manager');
$manager_email = get_field('resident_manager_email');
$manager_cell = get_field('resident_manager_phone');
$gform_id = 38; // the Gravity Form ID we're using
$prop_id = get_field('project_id');
$apply_online_link = 'http://lbpm.com/apartment-rental-application/intro/?pid=' . $prop_id;
$property_onshow = false;  //@TODO - add ability in post meta to turn this on.

// get the location of this building
$property_location = get_the_terms($post->ID,'property_location');
if ( $property_location && ! is_wp_error( $property_location ) ) {
	$property_location = array_shift($property_location);
	$keyword = $property_location->name. ' apartments';
	$location = '<a href="' . get_term_link($property_location) . '" title="' . $keyword . '">'. $keyword .'</a>';
} else {
	$location = "building";
}

// get the URL to the Podio maintenance webform for this property
$podio_maint_app_id = get_field('podio_maintenance_app_id');
$podio_maint_app_webform_id = get_field('podio_maintenance_app_webform_id');
if ( $podio_maint_app_id && $podio_maint_app_webform_id )
	$maintenance_request_url =  "https://podio.com/webforms/{$podio_maint_app_id}/{$podio_maint_app_webform_id}";
else
	$maintenance_request_url =  "http://lbpm.com/tenants/maintenance-request/";

$pay_rent_online_url = "http://lbpm.com/tenants/pay-rent-online/";
$leave_a_review_url = "http://www.reputation.com/kiosk?key=01cd013304f";

$comment_text = "These reviews are based on the experiences of actual residents. The reviews below may contain insights into management and community amenities, as well as neighborhood tips for living in our {$location}.";


?>

<!-- ** Primary Section ** -->
<section id="primary" class="content-full-width"><?php
	if( have_posts() ) :
		while( have_posts() ):
			the_post();?>

			<div class="property-single-detail">
				<ul class="single-property-info">
					<?php $address = get_post_meta ( $post->ID, "address",true);
						if( !empty($address) ):
							echo "<li class='price'>{$address}</li>";
						endif;

						the_terms($post->ID,'property_type','<li>'.__('Type','dt_themes').': ',', ','</li>');

						the_terms($post->ID,'property_location','<li>'.__('Location','dt_themes').': ',', ','</li>');

						if ($prop_id)
							echo '<li>'.__('Property ID',"dt_themes")." : <span>{$prop_id}</span></li>";

						if ($manager)
							echo '<li>'.__('Manager',"dt_themes")." : <span>{$manager}</span></li>";

						/*
						$hide_area = get_post_meta ( $post->ID, "_hide_area",true);
						$area = get_post_meta ( $post->ID, "_area",true);
						if( empty($hide_area) && !empty($area) ):
							$areaunit = dttheme_option("property","area_unit");
							echo '<li>'.__('Area',"dt_themes")." : <span>{$area} {$areaunit}</span></li>";
						endif;

						$hide_beds = get_post_meta ( $post->ID, "_hide_bedrooms",true);
						$beds = get_post_meta ( $post->ID, "_bedrooms",true);
						if( empty($hide_beds) && !empty($beds) ):
							echo '<li>'.__('Beds',"dt_themes")." : <span>{$beds}</span></li>";
						endif;

						$hide_baths = get_post_meta ( $post->ID, "_hide_bathrooms",true);
						$baths = get_post_meta ( $post->ID, "_bathrooms",true);
						if( empty($hide_baths) && !empty($baths) ):
							echo '<li>'.__('Baths',"dt_themes")." : <span>{$baths}</span></li>";
						endif;

						$hide_parking = get_post_meta ( $post->ID, "_hide_parking",true);
						$parking = get_post_meta ( $post->ID, "_parking",true);
						if( empty($hide_parking) && !empty($parking) ):
							echo '<li>'.__('Garage',"dt_themes")." : <span>{$parking}</span></li>";
						endif;

						$hide_floors = get_post_meta ( $post->ID, "_hide_floors",true);
						$floors = get_post_meta ( $post->ID, "_floors",true);
						if( empty($hide_floors) && !empty($floors) ):
							echo '<li>'.__('Floors',"dt_themes")." : <span>{$floors}</span></li>";
						endif;
						*/

						//echo "<li class='print-icon'><a href='javascript:window.print()'><i class='fa fa-print'></i></a></li>";
						?>


					<li class="print-icon addthis_toolbox addthis_default_style addthis_32x32_style">
						<span class="label">Share: </span>
						<a class="addthis_button_facebook"><i class='fa fa-facebook-square'></i></a>
						<a class="addthis_button_email"><i class='fa fa-envelope'></i></a>
						<a class="addthis_button_print"><i class='fa fa-print'></i></a>
						<a class="addthis_button_compact"><i class='fa fa-plus-square'></i></a>
						<a class="addthis_counter addthis_bubble_style"></a>
					</li>

					<script type="text/javascript">var addthis_config = {"data_track_addressbar":false};</script>
					<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-50a580ad0b7eed23"></script>
				</ul>

				<div class="property-gallery-container">
					<div id="property-video" class="column dt-sc-two-third no-space first">

						<?php // Top left ribbon shows on hover
						$contract_type = "";
						$contract_type_slug = "";
						$contract_type_link = "";

						$contract = get_the_terms( $post->ID, 'contract_type' );
						if( is_object( $contract) || is_array($contract) ){
							foreach ( $contract as $c ) :
								$contract_type = $c->name;
								$contract_type_slug = $c->slug;
								$contract_type_link = get_term_link( $contract_type_slug, 'contract_type' );
							endforeach;
						}

						if( !empty( $contract_type ) ):?>
							<span class="property-contract-type <?php echo $contract_type_slug;?>"><?php
								echo "<a href='{$contract_type_link}'>$contract_type</a>";?></span>
						<?php endif;

						// !Featured video
						$featured_video = get_post_meta ( $post->ID, "property_featured_video",true);
						if( !empty($featured_video) ): ?>

							<div class="dt-video-wrap">
								<?php echo wp_oembed_get( $featured_video ); ?>
							</div><?php

						elseif ( has_post_thumbnail() ): // check if the post has a Post Thumbnail assigned to it.
							the_post_thumbnail();
						else:
							echo "<li><img src='http://placehold.it/1060x718&text=Photo Coming Soon' alt='' title=''/></li>";
						endif;
						?>

					</div><!-- /#property-video -->

					<div id="single-property-enquiry-form" class="column dt-sc-one-third no-space">
						<div class="form-padding">
							<div class="icon"><i class="fa fa-mobile-phone fa-4x"></i></div>

							<div class="form-heading">
								<h3 class="form-title">Check Availability</h3>
								<p class="form-description">or call <a href="tel:<?php echo $phone; ?>"><?php echo $phone; ?></a></p>
							</div>

							<div class="form-body">
								<?php //load the gravity form we're using
									$hidden_fields = 'prop_id=' . urlencode($prop_id) .'&manager='. urlencode($manager);
									$hidden_fields .= '&manager_cell='. urlencode($manager_cell) . '&manager_email='. urlencode($manager_email);
									echo do_shortcode("[gravityform id=$gform_id title=false description=false ajax=true field_values=$hidden_fields]" );
								?>
							</div>

							<span class="rental-app-link"> -or- <a href="<?php echo $apply_online_link; ?>" class="dt-sc-button small orange">Start Rental Application</a></span>


						</div>

					</div><!-- /#property-info -->
				</div><!-- /#property-gallery-container -->

				<div class="clear"> </div>
				<div class="dt-sc-hr-invisible"> </div>

				<!-- !Tabs Begin -->

				<?php // Install Mate-tabs WP-plugin; doesn't support Multi-site (as per forums) http://codecanyon.net/item/mate-tabs-wordpress-plugin/4692611/comments ?>

				<div data-zlname="tabs1" class="zl_matetabs">
					<ul>
						<li class="zl_switch_to_nav"><i class="fa fa-info-circle"></i>Overview</li>
						<li class="zl_switch_to_nav"><i class="fa fa-calculator"></i>Pricing &amp; Availability</li>
						<li class="zl_switch_to_nav"><i class="fa fa-th-large"></i>Floorplans</li>
						<li class="zl_switch_to_nav"><i class="fa fa-map-marker"></i>Neighborhood Info</li>
						<li class="zl_switch_to_nav"><i class="fa fa-camera-retro"></i>Photos</li>
						<li class="zl_switch_to_nav"><i class="fa fa-comments"></i>Current Tenants</li>
					</ul>

				  <!-- !Overview Tab -->
				  <div class="zl_nav_trig zl_switch_to_nav"><i class="fa fa-info-circle"></i>Overview</div>
				  <section id="property-features-tab" style="display: block;">
						<?php the_content(); ?>

						<div class="clear"> </div>
						<div class="dt-sc-hr-invisible-small"> </div>

						<div class=""><!-- column dt-sc-one-half space first -->
						<?php // !Building Amenities
							$amenities_objs = get_the_terms( $post->ID,'property_amenities');
							if( is_array( $amenities_objs ) ) {

								echo '<h4 class="border-title"><span>'.__('Building Amenities','dt_themes').'</span></h4>';
								echo '<ul class="amenities-list dt-sc-fancy-list rounded-tick">';
								echo get_the_term_list( $post->ID, 'property_amenities', '<li>', '</li><li>', '</li>' );
								echo "</ul>";
							}
						?>
						</div>
						<div class=""><!-- column dt-sc-one-half space -->
						<?php // !Unit Features
							$features_objs = get_the_terms( $post->ID,'unit_features');
							if( is_array( $features_objs ) ) {
								echo '<h4 class="border-title"><span>'.__('Unit Features','dt_themes').'</span></h4>';
								echo '<ul class="amenities-list dt-sc-fancy-list rounded-tick">';
								echo get_the_term_list( $post->ID, 'unit_features', '<li>', '</li><li>', '</li>' );
								echo "</ul>";
							}
						?>
						</div>

						<p class="disclaimer">Features may vary by unit.</p>
				  </section>

				  <!-- !Pricing/Availability Tab -->
				  <div class="zl_nav_trig zl_switch_to_nav"><i class="fa fa-calculator"></i>Pricing &amp; Availability</div>
				  <section id="pricing-availability-tab">
						<?php if($property_onshow == 'true') { ?>
							<div class="dt-sc-success-box">
								<?php echo "<strong>Move In Special: </strong>" . $move_in_special; ?>
							</div>
						<?php } ?>

					 	<div class="pricing-availability-table">
							<?php // !Vacancies
								//@TODO - add a column to the shortcode for Contact
								//edit in lbpm_client_concierge shortcodes.php, line 153

								// TEMPORARY HACK TO SHOW MOVE-IN APPS FOR NORTHRIDGE PROPERTIES
								$northridge = array(1000.01,1000.02,1000.03,1000.04,1000.05,2190.08,9218);

								if (in_array($prop_id, $northridge))
								{
									echo do_shortcode( "[lbpm_show_items app_name='move-ins' view='table' prop_id='{$prop_id}' filter_field_key='status' filter_field_values='Vacant|App Pending' columns='Unit Reference|Type|Asking Rent|Security Deposit' cta='Start Rental Application' cta_link='{$apply_online_link}' ]" );

								}
								else
								{
									echo do_shortcode( "[lbpm_show_items app_name='vacancies' view='table' prop_id='{$prop_id}' filter_field_key='status' filter_field_values='Vacant' columns='Unit Reference|Type|Asking Rent|Security Deposit']" );

								}

							?>
					 	</div>
				  </section>
				  <!-- !Floorplans Tab -->
				  <div class="zl_nav_trig zl_switch_to_nav"><i class="fa fa-th-large"></i>Floorplans</div>
				  <section id="floorplans-tab">
						<?php // Uses new ACF Repeater field
						if(get_field('floorplans')): ?>

							<table class="floorplans" cellspacing="0" border="0">
								<thead>
									<tr>
										<th class="floorplan" colspan="2">Floorplan</th>
										<th class="bed">Bed</th>
										<th class="bath">Bath</th>
										<th class="sqft">SqFt</th>
										<th class="contact">&nbsp;</th>
									</tr>
								</thead>
								<tbody>

								<?php while(has_sub_field('floorplans')): ?>

									<?php //Setup contact message. @TODO Convert this to a Podio form
										$email_subject = "Info request for ". get_the_title() .", the " . get_sub_field('lbpm_floorplan_name') . " floorplan";
										$email_body = "Hi there,
										I'm interested in renting an apartment at ". get_the_title() .".	I'm looking at the " . get_sub_field('lbpm_floorplan_name') . " floorplan.

										Please call me back at ______.

										Thanks,
										YOUR NAME";

										$email_subject = rawurlencode($email_subject);
										$email_body = rawurlencode($email_body);
									?>

									<?php //Setup floorplan image
										$floorplan_image = get_sub_field('lbpm_floorplan_image');
									?>

									<tr>
										<td class="floorplan-thumb">
											<a target="_blank" href="<?php echo $floorplan_image[url]; ?>">
												<img src="<?php echo $floorplan_icon; ?>">
											</a>
										</td>
										<td class="floorplan">
											<a target="_blank" href="<?php echo $floorplan_image[url]; ?>"><?php the_sub_field('lbpm_floorplan_name'); ?></a>
										</td>
										<td class="bed"><?php the_sub_field('lbpm_beds'); ?></td>
										<td class="bath"><?php the_sub_field('lbpm_baths'); ?></td>
										<td class="size"><?php the_sub_field('lbpm_size'); ?></td>
										<td class="contact">
											<a class="button" href="<?php echo 'mailto:' . get_field('resident_manager_email') . '?subject=' . $email_subject . '&bcc=admin+propertyinquiries@lbpm.com&body=' . $email_body; ?>">Contact</a>
										</td>
									</tr>

								<?php endwhile; ?>

								</tbody>
							</table>

						<?php
							else: // no floorplans saved for this property
								echo "NO FLOORPLANS HAVE BEEN LOADED TO THIS PROPERTY YET. THEY'RE COMING SOON.";
							endif; ?>
				  </section>

				  <!-- !Maps/Neighborhood Tab -->
				  <div class="zl_nav_trig zl_switch_to_nav"><i class="fa fa-map-marker"></i>Maps &amp; Neighborhood Info</div>
				  <section id="map-neighborhood-tab">
						<?php

						//$gps = get_post_meta ( $post->ID, "_property_gps",true);
						//$gps = is_array($gps) ? $gps : array();

						//$address = get_post_meta ( $post->ID, "_property_address",true);
						$address = get_field('address') . ', ' . get_field('address2');

						// determine which map icon to use
						$property_type = get_the_terms( $post->ID, 'property_type' );
						if( is_object( $property_type) || is_array($property_type) ){
							foreach ( $property_type as $c ) :
				  					$icon = get_option( "taxonomy_term_$c->term_id" );
				  					$icon = $icon['icon'];
				  					$icon = !empty( $icon ) ? $icon : get_template_directory_uri().'/images/default-marker.png';
							endforeach;
						}else {
							$icon = get_template_directory_uri().'/images/default-marker.png';
						}?>

						<div class="map">

						 	<script type='text/javascript'>
								var ws_wsid = 'd271b3d32ed948a18067d3f3ef7037bf';
								var ws_address = '<?php echo $address; ?>';
// 								var ws_width = '100%';
								var ws_height = '540';
								var ws_layout = 'none';
								var ws_hide_footer = 'true';
								var ws_map_icon_type = 'custom';
								var ws_custom_pin = '<?php echo $icon; ?>';
								var ws_commute = 'true';
								var ws_transit_score = 'true';
								var ws_public_transit = 'true';
								var ws_show_reviews = 'true';
								var ws_map_modules = 'default';
								var ws_no_link_info_bubbles = 'true';
								var ws_no_link_score_description = 'true';
								var ws_hide_bigger_map = 'true';
								var ws_hide_scores_below = '50';
							</script>

							<div id='ws-walkscore-tile'></div>
							<script type='text/javascript' src='//www.walkscore.com/tile/show-walkscore-tile.php'></script>

						</div><!-- /.map -->
				  </section>

				  <!-- !Photos Tab -->
				  <div class="zl_nav_trig zl_switch_to_nav"><i class="fa fa-camera-retro"></i>Photo Gallery</div>
				  <section id="photo-gallery-tab">
						<?php //Create a WP gallery using the new ACF plugin

							$image_ids = get_field('property_photos', false, false);

							echo do_shortcode('[gallery ids="' . implode(',', $image_ids) . '" size="thumbnail" columns="4" link="file" ]');
						?>
				  </section>

				  <!-- !Current Tenants Tab -->
				  <div class="zl_nav_trig zl_switch_to_nav" style="border-bottom-style: none;"><i class="fa fa-comments"></i>Current Tenants</div>
				  <section id="reviews-tab">
						<div class="">
							<h2>It’s great to be an LBPM Resident at <?php the_title(); ?></h2>
							<p class="desc">Pay your rent online, setup auto-payments, and even submit maintenance requests with a few clicks. It’s just a few of the ways we’re working to make your life a little easier, and your home a little more comfy.</p>
							<ul class="dt-sc-fancy-list rounded-arrow">
								<li><strong>Responsive service</strong> – Got a maintenance issue? We’ll fix it ASAP.</li>
								<li><strong>Pay rent online</strong> – Quick. Easy. Paperless.</li>
								<li><strong>Building community</strong> – Special events. Contests. So much more than just an apartment.</li>
								<li><strong>Special deals</strong> – We’re able to negotiate volume discounts with many local vendors and retailers. Check with your resident manager to find out about programs in your area.</li>
							</ul>

							<div class="dt-sc-hr-invisible-small"></div>

							<a href="<?php echo $pay_rent_online_url; ?>" target="_blank" class="dt-sc-button large orange filled with-icon "><i class="fa fa-chevron-circle-right"> </i>Pay Rent Online</a>
							<a href="<?php echo $maintenance_request_url; ?>" target="_blank" title="LBPM Maintenance Request" class="dt-sc-button   large orange filled with-icon fancybox-iframe"><i class="fa fa-chevron-circle-right"> </i>Make a Maintenance Request</a>
							<a href="<?php echo $leave_a_review_url; ?>" target="_blank" class="dt-sc-button large  orange filled with-icon "><i class="fa fa-chevron-circle-right"> </i>Leave A Review</a>

							<div class="dt-sc-hr-invisible-medium"></div>
							<div class="dt-sc-clear"></div>

<!--
							<h3>New to LBPM? Need help getting your apartment set up?</h3>
							<p>Check out the links to the left (above) for helpful info. We’ve compiled a list of services and utilities, plus a comprehensive list of our most frequently asked questions.</p>
							<div class="social-bookmark"></div>
							<div class="social-share"></div>

							<h3><?php /* the_title(); ?> Resident Satisfaction</h3>
							<p><?php echo $comment_text; ?></p>
							<?php $comm = get_option('woo_comments');
							if ( 'open' == $post->comment_status && ($comm == "post" || $comm == "both") ) : ?>
								<?php comments_template('/comments-property.php', true); ?>
							<?php endif;*/ ?>
-->

						</div>
				  </section>
				</div> <!-- zl_matetabs -->

				<div class="clear"> </div>
				<div class="dt-sc-hr-invisible-small"> </div>

				<?php $agents = get_post_meta ( $post->ID, "_property_agents",true);
					$agents = is_array($agents) ? array_filter($agents) : array();
					$agents_email_ids = array();

					if( count($agents) > 0 ):
						echo '<div id="single-property-agent-info" class="column dt-sc-one-half first">';
							$prefix = $sufix = "";
							if( count($agents) > 1 ):
								echo '<h4>'.__('Agents Information','dt_themes').'</h4>';
								echo '<div class="dt-sc-agent-carousel-wrapper">';
								echo '	<ul class="dt-sc-agent-carousel">';
								$prefix = "<li>";
								$sufix = "</li>";
							else:
								echo '<h4>'.__('Agent Information','dt_themes').'</h4>';
							endif;

							foreach( $agents as $agent_id ):
								$agent = get_post($agent_id,'ARRAY_A');
								$link = get_permalink($agent_id);
								echo $prefix;
								echo '<div class="dt-sc-agents-list">';

									echo '	<div class="dt-sc-agent-thumb">';
									 		if( has_post_thumbnail($agent_id) ) :
									 			echo get_the_post_thumbnail($agent_id,"full");
									 		else:
									 			echo '<img src="http://placehold.it/400x420&text=Image" />';
									 		endif;
									echo '	</div>';

									echo '	<div class="dt-sc-agent-details">';
										echo "	 <h4><a href='{$link}'>{$agent['post_title']}</a></h4>";

										echo '	 <div class="dt-sc-agent-contact">';
										 			$agent_mobile = get_post_meta ( $agent_id,"_agent_mobile",true);
										 			$agent_phone = get_post_meta ( $agent_id,"_agent_phone",true);
										 			$agent_email_id = get_post_meta ( $agent_id,"_agent_email_id",true);
										 			array_push($agents_email_ids, $agent_email_id);

										 			if( !empty( $agent_mobile ) )
										 				echo "<p> <span class='fa fa-mobile'> </span> {$agent_mobile} </p>";

										 			if( !empty( $agent_phone ) )
										 				echo "<p> <span class='fa fa-phone'> </span> {$agent_phone} </p>";

										 			if( !empty( $agent_email_id ) )
										 				echo "<p> <span class='fa fa-envelope'> </span> <a href='mailto:{$agent_email_id}'>{$agent_email_id}</a></p>";
										echo '	 </div>';
									echo '	</div>';

									echo '	<div class="dt-sc-agent-content">'.do_shortcode($agent['post_content']).'</div>';

									$socials =	 get_post_meta( $agent_id, "_agent_social",true);
									if( !empty($socials) ) :
										echo '<ul class="dt-sc-social-icons">';
										foreach( $socials as $k => $v ):
											$i1 = IAMD_BASE_URL."images/sociable/hover/{$k}";
											$i2 = IAMD_BASE_URL."images/sociable/{$k}";
											$class = explode(".",$k);
											$class = $class[0];
											echo "<li class='{$class}'><a href='{$v}'><img src='{$i1}'/><img src='{$i2}'/></a></li>";
										endforeach;
										echo '</ul>';
									endif;
								echo '</div>';
								echo $sufix;
							endforeach;

							if( count($agents) > 1 ):
								echo '	</ul>';
								echo '<div class="carousel-arrows">';
								echo '	<a href="" class="agents-prev"> </a>';
								echo '	<a href="" class="agents-next"> </a>';
								echo '</div>';
								echo '</div>';
							endif;
						echo '</div>';

						echo '<div id="single-property-enquiry-form" class="column dt-sc-one-half">';
							echo '<h4>'.__(' Enquiry Form','dt_themes').'</h4>';?>

								<form id="property-enquiry" method="post" action="<?php echo get_template_directory_uri()."/framework/property-enquiry.php";?>">

									<div id="message"></div>

									<div class="column dt-sc-one-half first">
										<p>
											<label><?php _e('First Name','dt_themes');?></label>
											<input type="text" name="txtfname" required="required">
										</p>
									</div>

									<div class="column dt-sc-one-half">
										<p>
											<label><?php _e('Last Name','dt_themes');?></label>
											<input type="text" name="txtlname">
										</p>
									</div>

									<div class="column dt-sc-one-half first">
										<p>
											<label><?php _e('Email','dt_themes');?></label>
											<input type="email" name="txtemail" required="required">
										</p>
									</div>

									<div class="column dt-sc-one-half">
										<p>
											<label><?php _e('Phone','dt_themes');?></label>
											<input type="tel" name="phone">
										</p>
									</div>

										<p>
											 <label><?php _e('Message','dt_themes');?></label>
											 <textarea cols="" rows="" name="message" required="required"></textarea>
										</p>

										<input type="hidden" name="admin_emailid" value="<?php echo	get_option('admin_email');?>">
										<input type="hidden" name="txtproperty" value="<?php the_title();?>">
										<input type="hidden" name="property_link" value="<?php the_permalink();?>">
										<?php foreach( $agents_email_ids as $agent_email_id): ?>
											<input type="hidden" name="agents[]" value="<?php echo $agent_email_id;?>">
									<?php endforeach;?>
										<input type="submit" value="<?php _e('Send','dt_themes');?>">
								</form><?php
						echo '</div>';
					endif;?>


				</div><!-- /.property-single-detail -->
		<?php edit_post_link( __( ' Edit ','dt_themes' ) );
	endwhile;
endif;?>

<?php // !Related Properties Query
	$related = get_post_meta ( $post->ID, "_related_properties",true);
	$ptype	 = wp_get_object_terms( $post->ID, 'property_type');
	$ptypeid = is_array( $ptype ) && sizeof($ptype) > 0 ? $ptype[0]->term_id : $ptype;

	$location = wp_get_object_terms( $post->ID, 'property_location');
	$locationid = is_array( $location ) && sizeof($location) > 0 ? $location[0]->term_id : $location;

	if( !empty($related)  && !empty($ptypeid) ):
		$args = array(
			'orderby' => 'rand',
			'posts_per_page' => '3' ,
			'post_type' => 'dt_properties', // necessary?
			'post__not_in' => array($post->ID),
			'tax_query' => array(
				'relation' => 'AND',
				array(
					'taxonomy'=>'property_type',
					'field'=>'id',
					'operator'=>'IN',
					'terms'=>array($ptypeid),
				),
				array(
					'taxonomy'=>'property_location',
					'field'=>'id',
					'operator'=>'IN',
					'terms'=>array($locationid),
				),
			)
		);

	query_posts($args);
	if( have_posts() ): ?>
	<!-- !Related Properties -->
	<div id="dt-related-properties">
		<div class="clear"> </div>
		<div class="dt-sc-hr-invisible-medium"> </div>
		<h2 class="border-title"> <span><?php _e('Related Properties','dt_themes');?></span> </h2>

	<?php
		// @TODO - echo out this subtitle - "Similar Apartments in or near Northridge, CA"

		$count = 1;
		while( have_posts() ):
			the_post();
			$the_id = get_the_ID();
			$permalink = get_permalink($the_id);
			$title = get_the_title($the_id);

			$contract_type = "";
			$contract_type_slug = "";
			$contract = get_the_terms( $the_id, 'contract_type' );
			if( is_object( $contract) || is_array($contract) ){
				foreach ( $contract as $c ) :
					$contract_type = $c->name;
					$contract_type_slug = $c->slug;
					$contract_type_link = get_term_link( $contract_type_slug, 'contract_type' );
				endforeach;
			}

			$property_type = "";
			$property_type_link = "";
			$property_type = get_the_terms( $the_id, 'property_type' );
			if( is_object( $property_type) || is_array($property_type) ){
				foreach ( $property_type as $c ) :
					$property_type = $c->name;
					$property_type_link = get_term_link( $property_type, 'property_type' );
				endforeach;
			}

			$media = get_post_meta ( $the_id, "_property_media",true);
			$media = is_array($media) ? $media : array();

			$price = get_post_meta ( $the_id, "_property_price",true);
			$first = ( $count === 1 ) ? " first" : "";?>
			<!-- Property Item -->
			<div class="column dt-sc-one-third <?php echo $first;?>">
				<div class="property-item">

					<div class="property-thumb"><?php
						/*if( !empty( $contract_type ) ):?>
							<span class="property-contract-type <?php echo $contract_type_slug;?>"><?php
								echo "<a href='{$contract_type_link}'>$contract_type</a>";?></span><?php
						endif;*/

						if ( has_post_thumbnail() ) : ?>
							<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
								<?php the_post_thumbnail('thumbnail'); ?>
							</a>
						<?php else: ?>
							<img src='http://placehold.it/338x200&text=Photo Not Available' alt='' title=''/>
						<?php endif; ?>

						<div class="property-thumb-meta"><?php /*
							if( !empty($property_type) )
								echo "<span class='property-type'><a href='{$property_type_link}'>{$property_type}</a></span>";

							if( !empty($price) )
								echo "<span class='property-price'>{$currency} {$price} </span>"; */?>
						</div>
					</div>

					<div class="property-details">
						<div class="property-details-inner">
							<h2><a href='<?php echo $permalink;?>'><?php echo $title;?></a></h2>
							<h3><?php echo get_post_meta ( $the_id, "address2",true);?></h3>
							<ul class="property-meta"><?php
								/*$hide_area = get_post_meta ( $the_id, "_hide_area",true);
								$area = get_post_meta ( $the_id, "_area",true);

								if( empty($hide_area) && !empty($area) ):
									$areaunit = dttheme_option("property","area_unit");
									echo '<li>'.__('Area',"dt_themes")." : <span>{$area} {$areaunit}</span></li>";
								endif;

								$hide_beds = get_post_meta ( $the_id, "_hide_bedrooms",true);
								$beds = get_post_meta ( $the_id, "_bedrooms",true);
								if( empty($hide_beds) && !empty($beds) ):
									echo '<li>'.__('Beds',"dt_themes")." : <span>{$beds}</span></li>";
								endif;

								$hide_baths = get_post_meta ( $the_id, "_hide_bathrooms",true);
								$baths = get_post_meta ( $the_id, "_bathrooms",true);
								if( empty($hide_baths) && !empty($baths) ):
									echo '<li>'.__('Baths',"dt_themes")." : <span>{$baths}</span></li>";
								endif;

								$hide_parking = get_post_meta ( $the_id, "_hide_parking",true);
								$parking = get_post_meta ( $the_id, "_parking",true);
								if( empty($hide_parking) && !empty($parking) ):
										echo '<li>'.__('Garage',"dt_themes")." : <span>{$parking}</span></li>";
								endif;

								$hide_floors = get_post_meta ( $the_id, "_hide_floors",true);
								$floors = get_post_meta ( $post->ID, "_floors",true);
								if( empty($hide_floors) && !empty($floors) ):
									echo '<li>'.__('Floors',"dt_themes")." : <span>{$floors}</span></li>";
								endif;*/?>
							</ul>
						</div>
					</div>
				</div>
			</div><?php
			$count++;
		endwhile; ?>
	</div><?php
	endif; ?>
	<!-- Related Properties End -->
	<?php endif;?>
</section>

<?php get_footer(); ?>