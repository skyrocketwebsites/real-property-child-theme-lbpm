<?php
/**
 * dttheme_subtitle_section function.
 *
 * This function overrides the parent function, located in /realproperty/framework/register_public.php (line 862)
 * We had to override the entire function, since there's no good place for a hook.
 *
 * @author Nick Daugherty
 * @author Christian Repato
 *
 * @param int $id (default: 0) -- This is the WordPress ID of the post
 * @param mixed $type -- This is the type of post/page/property/etc
 * @param array $settings (default: array())
 * @return void -- Doesn't return any values. Only echos them out.
 */
function dttheme_subtitle_section($id=0,$type,$settings = array() ){

	if( $id > 0 ){

		$title = get_the_title($id);

		if( $type === "post" )
			$settings = '_dt_post_settings';
		elseif( $type === "page" || $type === "dt_properties" || $type === "dt_agents" || $type === "dt_agencies" )
			$settings = '_tpl_default_settings';
		elseif( $type === "dt_portfolios" )
			$settings = '_portfolio_settings';

		$settings = get_post_meta( $id, $settings, TRUE );
		$settings = is_array($settings) ? $settings : array();
	}

	$bg = array_key_exists('sub-title-bg', $settings) ? $settings['sub-title-bg'] :IAMD_BASE_URL.'images/parallax-building.jpg';
	$position = array_key_exists('sub-title-bg-position', $settings) ? $settings['sub-title-bg-position'] :'center center';
	$repeat = array_key_exists('sub-title-bg-repeat', $settings) ? $settings['sub-title-bg-repeat'] :'repeat';
	$style = "background:url($bg) $position $repeat";
	$darkbg = array_key_exists('dark-bg', $settings) ? "dark-bg" : "";
	$custompagetitlemain = array_key_exists('custom-page-title-main', $settings) ? $settings['custom-page-title-main'] : "";
	$custompagetitlesub = array_key_exists('custom-page-title-sub', $settings) ? $settings['custom-page-title-sub'] : "";

	if ( !array_key_exists('show_slider', $settings) && !array_key_exists('hide-page-title', $settings) ) :

		echo '<!-- ** Title Fullwidth Background ** -->';
		echo "<section class='fullwidth-background {$darkbg}' style='{$style}'>";
		echo '	<div class="fullwidth-background-wrapper">';
		echo '		<div class="container">';
		echo '			<div class="main-title-section">';
		echo '				<h1 class="title">';

		echo ( array_key_exists('custom-page-title-main', $settings) ) ? $custompagetitlemain : $title;
		echo ( array_key_exists('custom-page-title-sub', $settings) ) ? "<span class='sub-heading'>{$custompagetitlesub}</span>" : "";

		echo "</h1>";
							new dttheme_breadcrumb;
		echo '			</div>';
		echo '		</div>';
		echo '	</div>';
		echo '</section><!-- ** Title Fullwidth Background End ** -->';
	endif;
}
