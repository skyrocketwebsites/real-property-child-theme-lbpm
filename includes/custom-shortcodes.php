<?php
// This file is for custom shortcodes that overwrite the original ones from the RealProperty theme

// Register a new shortcode: [dt_sc_toggle_framed title="xxx" background="xxx"]
add_shortcode( 'dt_sc_toggle_framed', 'dt_sc_toggle_framed' );
function dt_sc_toggle_framed($attrs, $content = null) {
    // load our custom colors stylesheet
    wp_enqueue_style('shortcode-colors');

	extract ( shortcode_atts ( array (
			'title' => '',
			'background' => '',
	), $attrs ) );

	$out = '<div class="dt-sc-toggle-frame">';
	$out .= "	<h5 class='dt-sc-toggle {$background}'><a href='#'>{$title}</a></h5>";
	$out .= '	<div class="dt-sc-toggle-content" style="display: none;">';
	$out .= '		<div class="block">';
	$out .=              do_shortcode($content);
	$out .= '		</div>';
	$out .= '	</div>';
	$out .= '</div>';
	return $out;
}

add_shortcode( 'dt_sc_service', 'dt_sc_service' );
function dt_sc_service( $attrs, $content = "" ){
	extract ( shortcode_atts ( array ( 'title' => '', 'title_icon' => '', 'link'=>'#', 'image'=>'' ), $attrs ) );

	$content = DTCoreShortcodesDefination::dtShortcodeHelper ( $content );

	$out = "<div class='dt-sc-services'><a href='{$link}'>";
	$out .= '<h4>';
	$out .= ( !empty($title_icon) ) ? "<span class='fa {$title_icon}'></span>" : "";
	$out .= ( !empty($title) ) ? "{$title}":"";
	$out .= '</h4>';
	$out .= ( !empty($image) ) ? "<img src='{$image}' alt='{$title}'/>":"";
	$out .= $content;
	$out .= '</a></div>';
	return $out;
}

add_shortcode( 'dt_sc_callout_box', 'dt_sc_callout_box' );
function dt_sc_callout_box($attrs, $content = null) {
	extract ( shortcode_atts ( array (
			'type' => "type1",
			'link' => '#',
			'button_text'=> __('Purchase Now','dt_themes'),
			'icon' =>'',
			'target' => '',
			'class' => '',
			'button_class' => '',
	), $attrs ) );

	$attribute = !empty($icon) ? "class='dt-sc-callout-box with-icon {$type} {$class}' " :" class='dt-sc-callout-box {$type} {$class}' ";

	$target = empty($target) ? 'target="_blank"' : "target='{$target}' ";

	$content = DTCoreShortcodesDefination::dtShortcodeHelper ( $content );

	$out = "<div {$attribute}>";
	$out .= ( !empty( $title ) ) ? "<h2>{$title}</h2>" : "";
	$out .= '<div class="column dt-sc-four-fifth first">';
	if( !empty( $icon ) ):
		$out .= '<div class="icon">';
		$out .= "<span class='fa {$icon}'></span>";
		$out .= '</div>';
	endif;
	$out .= $content;
	$out .= '</div>';

	$out .= '<div class="column dt-sc-one-fifth">';
	$out .= ( !empty($link) ) ? "<a href='{$link}' class='dt-sc-button small {$button_class}' {$target}>{$button_text}</a>" : "";
	$out .= '</div>';
	$out .= "</div>";

	return $out;
}

add_shortcode( 'dt_sc_pricing_table_item', 'dt_sc_pricing_table_item' );
function dt_sc_pricing_table_item($attrs, $content = null) {
	extract ( shortcode_atts ( array (
			'heading' => __ ( "Heading", 'dt_themes' ),
			'per' => 'month',
			'price' => '',
			'button_link' => '#',
			'button_text' => __ ( "Buy Now", 'dt_themes' ),
			'button_size' => "small",
			'button_class' => '',
	), $attrs ) );

	$selected = (isset ( $attrs [0] ) && trim ( $attrs [0] == 'selected' )) ? 'selected' : '';

	$button_link= do_shortcode($button_link);

	$content = DTCoreShortcodesDefination::dtShortcodeHelper ( $content );
	$content = str_replace ( '<ul>', '<ul class="dt-sc-tb-content">', $content );
	$content = str_replace ( '<ol>', '<ul class="dt-sc-tb-content">', $content );
	$content = str_replace ( '</ol>', '</ul>', $content );
	$price = ! empty ( $price ) ? "<div class='dt-sc-price'> $price <span> $per</span> </div>" : "";

	$out = "<div class='dt-sc-pr-tb-col $selected'>";
	$out .= '	<div class="dt-sc-tb-header">';
	$out .= '		<div class="dt-sc-tb-title">';
	$out .= "			<h5>$heading</h5>";
	$out .= '		</div>';
	$out .= $price;
	$out .= '	</div>';
	$out .= $content;
	$out .= '<div class="dt-sc-buy-now">';

	if(preg_match('#^{{#', $button_link) === 1) {
		$button_link =  str_replace ( '{{', '[', $button_link );
		$button_link =  str_replace ( '}}', '/]', $button_link );
		$button_link = do_shortcode($button_link);
	}else{
		$button_link = esc_url ( $button_link );
	}

	$out .= do_shortcode ( "[dt_sc_button class='$button_class' size='$button_size' link='$button_link']" . $button_text . "[/dt_sc_button]" );
	$out .= '</div>';
	$out .= '</div>';
	return $out;
}
