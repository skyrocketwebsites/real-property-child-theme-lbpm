<?php

/**
 * lbpm_theme_features action
 *
 * Add theme support for Featured Images
 * The parent theme explicitly declares which post types get thumbnails.  So we have to override it.
 */
add_action('after_setup_theme', 'lbpm_theme_features', 11);
function lbpm_theme_features() {
	add_theme_support('post-thumbnails', array(
		'post',
		'page',
		'dt_agents',
		'dt_agencies',
		'dt_properties',
		'vendor',
	));

}

/**
 * lbpm_add_page_meta filter.
 *
 * We added our own custom filter hook to the parent theme inside the page_meta_save() function,
 * located in /realproperty/framework/theme_metaboxes/page_metabox.php (line 682)
 * This filter adds our custom page meta values to the parent theme, for enabling show/hide of
 * page title and using a custom title/subtitle.
 *
 * @author Nick Daugherty
 *
 * @param mixed $settings -- This is what gets passed in from the parent theme
 */
add_filter( 'page_meta_save_custom', 'lbpm_add_page_meta', 10 );

function lbpm_add_page_meta( $settings ) {

	$settings['custom-page-title-main'] = $_POST['custom-page-title-main'];
	$settings['custom-page-title-sub'] = $_POST['custom-page-title-sub'];
	$settings['hide-page-title'] = $_POST['hide-page-title'];

	return $settings;
}

/**
 * lbpm_add_page_metaboxes action
 *
 * We added our own custom action hook to the parent theme inside the page_settings() function,
 * located in /realproperty/framework/theme_metaboxes/page_metabox.php (line 222)
 * This action injects our custom HTML to the parent theme, for adding our new metaboxes for
 * show/hide of page title and using a custom title/subtitle.
 *
 * @author Nick Daugherty
 *
 * @param mixed $tpl_default_settings -- This is what gets passed in from the parent theme
 */

add_action( 'page_meta_box_custom', 'lbpm_add_page_metaboxes' );

function lbpm_add_page_metaboxes( $tpl_default_settings ) {
	?>
	<!-- 0. Hide Page Title Start (SKYROCKET ADDED) -->
	<div id="page-hide-title" class="custom-box">
		<div class="column one-sixth"><label><?php _e('Hide Page Title','dt_themes');?></label></div>
		<div class="column five-sixth last"><?php
			$switchclass = array_key_exists("hide-page-title",$tpl_default_settings) ? 'checkbox-switch-on' :'checkbox-switch-off';
			$checked = array_key_exists("hide-page-title",$tpl_default_settings) ? ' checked="checked"' : '';?>

			<div data-for="mytheme-hide-page-title" class="checkbox-switch <?php echo $switchclass;?>"></div>
			<input id="mytheme-hide-page-title" class="hidden" type="checkbox" name="hide-page-title" value="true"	<?php echo $checked;?>/>
			<p class="note"> <?php _e('Yes! to hide the Page title on this page.','dt_themes');?> </p>
		 </div>
	 </div><!-- Show Page Title End-->

	<div class="sub-title custom-box">
		<div class="column one-sixth"><label><?php _e( 'Custom Page Title','dt_themes');?></label></div>
		<div class="column five-sixth last">

			<?php $custompagetitlemain = array_key_exists ( "custom-page-title-main", $tpl_default_settings ) ? $tpl_default_settings ['custom-page-title-main'] : '';?>
			<input name="custom-page-title-main" type="text" class="large" value="<?php echo $custompagetitlemain;?>"/>
			<p class="note"><?php _e("OPTIONAL: Enter your desired title for this page. If this field is not completed, the normal page title will appear.",'dt_themes');?></p>

		</div>
	</div><!-- Custom Page Title End-->

	<div class="sub-title custom-box">
		<div class="column one-sixth"><label><?php _e( 'Page Sub-Title','dt_themes');?></label></div>
		<div class="column five-sixth last">

			<?php $custompagetitlesub = array_key_exists ( "custom-page-title-sub", $tpl_default_settings ) ? $tpl_default_settings ['custom-page-title-sub'] : '';?>
			<input name="custom-page-title-sub" type="text" class="large" value="<?php echo $custompagetitlesub;?>"/>
			<p class="note"><?php _e("OPTIONAL: Enter your desired sub-title for this page.",'dt_themes');?></p>

		</div>
	</div><!-- Page Sub-Title End-->

	<?php
}

/**
 * lbpm_fix_vip_vendor_breadcrumb filter.
 *
 * This filter tweaks the VIP vendor breadcrumbs, and was much more complicated than it should have been.
 *
 * @author Nick Daugherty
 *
 * @param mixed $term -- This is the vendor category that gets passed in
 */
//add_filter( 'vendor_category_breadcrumb', 'lbpm_fix_vip_vendor_breadcrumb', 10 );

function lbpm_fix_vip_vendor_breadcrumb( $term ) {

/*
	// vip resources page
	$vip_resources_page = get_page_by_path('clients/vip-resources');

	if($vip_resources_page->post_parent) {
		$vip_query = get_post($vip_resources_page->post_parent);
		//$this->simple_breadcrumb_case($vip_query);
		$link = '<a href="'.get_permalink($vip_query->ID).'">';
		$link .= ''. get_the_title($vip_query->ID) . '</a>'. $markup;
		echo $link;
	}
*/

	//$parents = get_post_ancestors( $post->ID );

	return $term;
}


/*-----------------------------------------------------------------------------------*/
/* Breadcrumb filter for VIP Resources page */
/*-----------------------------------------------------------------------------------*/
function remove_home_from_breadcrumb($links) {
	if ( $links[0]['url'] == get_home_url() ) {
		$links[0]['url'] = 'http://lbpm.com/clients/vip-resources/';
		$links[0]['text'] = 'VIP Resources';
	}

	if (is_singular('vendor')) {
		$vendor_categories = get_the_terms($post->ID, 'vendor_category');
		if ($vendor_categories) {
			$vendor_category = array_shift($vendor_categories);
			$new_link = array();
			$new_link[0]['text'] = $vendor_category->name;
			$new_link[0]['url'] = get_term_link($vendor_category);
			array_splice($links, -1, 0, $new_link ); // inject our array one before the end
		}
	}

	return $links;
}


